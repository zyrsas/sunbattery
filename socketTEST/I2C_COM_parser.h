/**
 * I2C_COM_parser.h
 *
 *  Created on: 19.02.2014
 *      Author: dpo
 */

#ifndef I2C_COM_PARSER_H_
#define I2C_COM_PARSER_H_
typedef int uint8_t;
typedef int uint16_t;
typedef int uint32_t;

uint16_t i2c_com_parse(uint8_t log_change);
uint16_t i2c_com_prepare_data(void);
uint16_t get_variable_index(uint32_t id);
uint32_t get_variable_value(uint16_t idx);
void set_variable_value(uint32_t id);

extern const uint32_t var_ids[];
extern const uint16_t var_size[];
extern const uint32_t var_addr[];
extern uint16_t com_flash_parameters;
extern char svnversion[16];
extern char svnversion_factory[16];

#endif /* I2C_COM_PARSER_H_ */
