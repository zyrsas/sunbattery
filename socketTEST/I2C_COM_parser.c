/**
 * @addtogroup i2c_com_parser
 * @{
 * I2C_COM_parser.c
 * @brief List of all programmable parameters, readable measurements. Access functions. Help functions for COM protocol (Android App support).
 *
 *  Created on: 19.02.2014
 *      Author: dpo
 */

#include "I2C_COM_parser.h"
#include "online_osci.h"


/** Variables ID's */
#define INSERT_VAR_IDS_HERE
const uint32_t var_ids[] = { // this line can be edited
0x104EB6A, // 0: rb485.f_grid[2]
0x16109E1, // 1: grid_mon[0].u_over.time
0x19C0B60, // 2: cs_neg[2]
0x31A6110, // 3: energy.e_ext_month
0x39BDE11, // 4: hw_test.state
0x5C7CFB1, // 5: logger.day_egrid_load_log_ts
0x64E4340, // 6: logger.minutes_ubat_log_ts
0x6A9FFA2, // 7: battery.charged_amp_hours
0x6E03755, // 8: wifi.ip
0x7367B64, // 9: rb485.phase_marker
0x7C61FAD, // 10: adc.u_ref_1_5v[0]
0x8679611, // 11: net.id
0x86C75B0, // 12: battery.stack_software_version[3]
0x95AFAA8, // 13: logger.minutes_ul3_log_ts
0xA04CA7F, // 14: g_sync.u_zk_n_avg
0xAA372CE, // 15: p_rec_req[1]
0xAFDD6CF, // 16: acc_conv.i_acc_lp_fast
0xBA16A10, // 17: wifi.sockb_protocol
0xC588B75, // 18: energy.e_ext_day_sum
0xCB5D21B, // 19: dc_conv.dc_conv_struct[1].p_dc_lp
0xCBA34B9, // 20: nsm.u_q_u[3]
0xDE3D20D, // 21: battery.status2
0xDF164DE, // 22: logger.day_eb_log_ts
0xDF45696, // 23: io_board.io1_polarity
0xE0505B4, // 24: flash_rtc.time_stamp_set
0xE799A56, // 25: io_board.rse_table[0]
0xF28E2E1, // 26: energy.e_ext_total_sum
0xFA29566, // 27: logger.minutes_ub_log_ts
0xFB40090, // 28: io_board.check_rs485_result
0x10842019, // 29: nsm.cos_phi_p[3][1]
0x1089ACA9, // 30: nsm.u_q_u[0]
0x108FC93D, // 31: max_phase_shift
0x10970E9D, // 32: energy.e_ac_month
0x1156DFD0, // 33: power_mng.battery_power
0x126ABC86, // 34: energy.e_grid_load_month
0x132AA71E, // 35: logger.minutes_temp2_log_ts
0x1348AB07, // 36: battery.cells[4]
0x147E8E26, // 37: g_sync.p_ac[1]
0x14C0E627, // 38: wifi.password
0x14FCA232, // 39: nsm.rpm_lock_out_power
0x162491E8, // 40: battery.module_sn[5]
0x16A1F844, // 41: battery.bms_sn
0x16B28CCA, // 42: adc.u_ref_1_5v[1]
0x173D81E4, // 43: rb485.version_boot
0x19608C98, // 44: partition[3].last_id
0x19B814F2, // 45: logger.year_egrid_feed_log_ts
0x1ABA3EE8, // 46: p_rec_req[0]
0x1AC87AA0, // 47: g_sync.p_ac_load_sum_lp
0x1B39A3A3, // 48: battery.bms_power_version
0x1B5445C4, // 49: io_board.check_rse_result
0x1BFA5A33, // 50: energy.e_grid_load_total_sum
0x1C4A665F, // 51: grid_pll[0].f
0x1D0623D6, // 52: wifi.dns_address
0x1D2994EA, // 53: power_mng.soc_charge_power
0x1D49380A, // 54: logger.minutes_eb_log_ts
0x1E5FCA70, // 55: battery.maximum_charge_current
0x1FEB2F67, // 56: switch_on_cond.u_min
0x21879805, // 57: logger.minutes_eac1_log_ts
0x21961B58, // 58: battery.current
0x21E1A802, // 59: energy.e_dc_month_sum[1]
0x21EE7CBB, // 60: rb485.u_l_grid[2]
0x2266DCB8, // 61: flash_rtc.rtc_mcc_quartz_max_diff
0x234B4736, // 62: fault[1].flt
0x234DD4DF, // 63: switch_on_cond.f_min
0x23AB8D0F, // 64: net.p_ac_ext_sum
0x24150B85, // 65: g_sync.u_zk_sum_mov_avg
0x241F1F98, // 66: energy.e_dc_day_sum[1]
0x2545E22D, // 67: g_sync.u_l_rms[2]
0x257B7612, // 68: battery.module_sn[3]
0x26260419, // 69: nsm.cos_phi_p[1][0]
0x26EFFC2F, // 70: energy.e_grid_feed_year
0x2703A771, // 71: cs_struct.is_tuned
0x27650FE2, // 72: rb485.version_main
0x27BE51D9, // 73: g_sync.p_ac_sc[0]
0x27C828F4, // 74: energy.e_grid_feed_total_sum
0x29BDA75F, // 75: display_struct.brightness
0x29CA60F8, // 76: io_board.rse_table[10]
0x2A449E89, // 77: logger.year_log_ts
0x2AE703F2, // 78: energy.e_dc_day[0]
0x2BC1E72B, // 79: battery.discharged_amp_hours
0x2E0C6220, // 80: io_board.home_relay_sw_off_delay
0x2ED89924, // 81: db.power_board.afi_t300
0x2F0A6B15, // 82: logger.month_ea_log_ts
0x2F3C1D7D, // 83: energy.e_load_day
0x3044195F, // 84: grid_mon[1].u_under.time
0x315D1490, // 85: power_mng.bat_empty_full
0x32CD0DB3, // 86: nsm.cos_phi_p[0][1]
0x34ECA9CA, // 87: logger.year_eb_log_ts
0x360BDE8A, // 88: nsm.startup_grad
0x3623D82A, // 89: prim_sm.island_flag
0x365D12DA, // 90: p_rec_req[2]
0x36A9E9A6, // 91: power_mng.use_grid_power_enable
0x37F9D5CA, // 92: fault[0].flt
0x381B8BF9, // 93: battery.soh
0x383A3614, // 94: db.power_board.afi_i60
0x3903A5E9, // 95: flash_rtc.flag_time_auto_switch
0x3906A1D0, // 96: logger.minutes_eext_log_ts
0x3A3050E6, // 97: grid_lt.threshold
0x3A444FC6, // 98: g_sync.s_ac_lp[0]
0x3A873343, // 99: energy.e_ac_day_sum
0x3A9D2680, // 100: energy.e_ext_year_sum
0x3AA565FC, // 101: net.package
0x3AFEF139, // 102: prim_sm.is_thin_layer
0x3B0C6A53, // 103: bat_mng_struct.profile_pdc_max
0x3B5F6B9D, // 104: rb485.f_wr[0]
0x3B7FCD47, // 105: fault[2].flt
0x3C705F61, // 106: io_board.rse_table[8]
0x3C87C4F5, // 107: energy.e_grid_feed_day
0x3CB1EF01, // 108: grid_mon[0].u_under.threshold
0x3DBCC6B4, // 109: io_board.rse_table[6]
0x3E722B43, // 110: grid_mon[1].f_under.threshold
0x400F015B, // 111: g_sync.p_acc_lp
0x4077335D, // 112: g_sync.s_ac_lp[1]
0x40FF01B7, // 113: battery.cells[6]
0x431509D1, // 114: logger.month_eload_log_ts
0x43257820, // 115: g_sync.p_ac[0]
0x437B8122, // 116: rb485.available
0x4397D078, // 117: nsm.cos_phi_p[1][1]
0x43CD0B6F, // 118: nsm.pf_delay
0x43F16F7E, // 119: flash_state
0x43FF47C3, // 120: db.power_board.afi_t60
0x44D4C533, // 121: energy.e_grid_feed_total
0x4539A6D4, // 122: can_bus.bms_update_response[0]
0x46892579, // 123: flash_param.write_cycles
0x474F80D5, // 124: iso_struct.Rn
0x485AD749, // 125: g_sync.u_ptp_rms[1]
0x488052BA, // 126: logger.minutes_ul2_log_ts
0x48D73FA5, // 127: g_sync.i_dr_lp[2]
0x495BF0B6, // 128: energy.e_dc_year_sum[0]
0x4B51A539, // 129: battery.prog_sn
0x4BC0F974, // 130: buf_v_control.power_reduction_max_solar
0x4BE02BB7, // 131: energy.e_load_day_sum
0x4C12C4C7, // 132: cs_neg[1]
0x4C14CC7C, // 133: logger.year_ea_log_ts
0x4C2A7CDC, // 134: nsm.cos_phi_p[2][1]
0x4C374958, // 135: nsm.startup_grad_after_fault
0x4DB1B91E, // 136: switch_on_cond.f_max
0x4E0C56F2, // 137: flash_rtc.rtc_mcc_quartz_ppm_difference
0x4E3CB7F8, // 138: phase_3_mode
0x4E49AEC5, // 139: g_sync.p_ac_sum
0x4E699086, // 140: battery.module_sn[4]
0x4E9D95A6, // 141: logger.year_eext_log_ts
0x4EE8DB78, // 142: energy.e_load_year_sum
0x4F330E08, // 143: io_board.io2_usage
0x4F735D10, // 144: db.temp2
0x508FCE78, // 145: adc.u_ref_1_5v[3]
0x50B441C1, // 146: logger.minutes_ea_log_ts
0x5293B668, // 147: logger.minutes_soc_log_ts
0x5411CE1B, // 148: logger.minutes_ul1_log_ts
0x5438B68E, // 149: grid_mon[1].u_over.threshold
0x54829753, // 150: p_rec_lim[1]
0x54B4684E, // 151: g_sync.u_l_rms[1]
0x54DBC202, // 152: io_board.rse_table[12]
0x554D8FEE, // 153: logger.minutes_eac2_log_ts
0x5570401B, // 154: battery.stored_energy
0x55C22966, // 155: g_sync.s_ac[2]
0x5673D737, // 156: wifi.connect_to_wifi
0x57429627, // 157: wifi.authentication_method
0x5867B3BE, // 158: io_board.rse_table[2]
0x58C1A946, // 159: io_board.check_state
0x59358EB2, // 160: power_mng.maximum_charge_voltage
0x5939EC5D, // 161: battery.module_sn[6]
0x5952E5E6, // 162: wifi.mask
0x5A316247, // 163: wifi.mode
0x5B10CE81, // 164: power_mng.is_heiphoss
0x5BB8075A, // 165: dc_conv.dc_conv_struct[1].u_sg_lp
0x5BD2DB45, // 166: io_board.io1_s0_imp_per_kwh
0x5CD75669, // 167: db.power_board.afi_t150
0x5D0CDCF0, // 168: p_rec_available[2]
0x5D34D09D, // 169: logger.month_egrid_load_log_ts
0x5E942C62, // 170: dc_conv.dc_conv_struct[1].mpp.fixed_voltage
0x5EE03C45, // 171: io_board.alarm_home_relay_mode
0x5F33284E, // 172: prim_sm.state
0x6002891F, // 173: g_sync.p_ac_sc_sum
0x60A9A532, // 174: logger.day_eext_log_ts
0x612F7EAB, // 175: g_sync.s_ac[1]
0x6279F2A3, // 176: db.power_board.version_boot
0x62B8940B, // 177: dc_conv.start_voltage
0x62D645D9, // 178: battery.cells[5]
0x62FBE7DC, // 179: energy.e_grid_load_total
0x63476DBE, // 180: g_sync.u_ptp_rms[0]
0x6388556C, // 181: battery.stack_software_version[0]
0x6476A836, // 182: dc_conv.dc_conv_struct[0].mpp.enable_scan
0x650C1ED7, // 183: g_sync.i_dr_eff[1]
0x65A44A98, // 184: flash_mem
0x65B624AB, // 185: energy.e_grid_feed_month
0x65EED11B, // 186: battery.voltage
0x664A1326, // 187: io_board.rse_table[14]
0x669D02FE, // 188: logger.minutes_eac_log_ts
0x6709A2F4, // 189: energy.e_ac_year_sum
0x67BF3003, // 190: display_struct.display_dir
0x682CDDA1, // 191: power_mng.battery_type
0x6830F6E4, // 192: io_board.rse_table[9]
0x68BA92E1, // 193: io_board.io2_s0_imp_per_kwh
0x68BC034D, // 194: parameter_file
0x68EEFD3D, // 195: energy.e_dc_total[1]
0x6974798A, // 196: battery.stack_software_version[6]
0x69AA598A, // 197: can_bus.requested_id
0x69B8FF28, // 198: battery.cells[2]
0x6B5A56C2, // 199: logger.month_eb_log_ts
0x6BA10831, // 200: db.power_board.afi_i30
0x6C243F71, // 201: modbus.address
0x6C2D00E4, // 202: io_board.rse_table[1]
0x6C44F721, // 203: i_dc_max
0x6CFCD774, // 204: energy.e_dc_year_sum[1]
0x6D5318C8, // 205: cs_map[1]
0x6E1C5B78, // 206: g_sync.p_ac_lp[1]
0x6E491B50, // 207: battery.maximum_charge_voltage
0x6F3876BC, // 208: logger.error_log_time_stamp
0x6FB2E2BF, // 209: db.power_board.afi_i150
0x6FD36B32, // 210: rb485.f_wr[1]
0x6FF4BD55, // 211: energy.e_ext_month_sum
0x701A0482, // 212: dc_conv.dc_conv_struct[0].enabled
0x70A2AF4F, // 213: battery.bat_status
0x70BD7C46, // 214: logger.year_eac_log_ts
0x70E28322, // 215: grid_mon[0].f_under.time
0x715C84A1, // 216: adc.u_ref_1_5v[2]
0x71765BD8, // 217: battery.status
0x71E10B51, // 218: g_sync.p_ac_lp[0]
0x7232F7AF, // 219: nsm.apm
0x7268CE4D, // 220: battery.inv_cmd
0x72ACC0BF, // 221: logger.minutes_ua_log_ts
0x7301A5A7, // 222: flash_rtc.time_stamp_factory
0x73489528, // 223: battery.module_sn[2]
0x742966A6, // 224: db.power_board.afi_i300
0x7689BE6A, // 225: io_board.home_relay_sw_on_delay
0x76C9A0BD, // 226: logger.minutes_soc_targ_log_ts
0x76CAA9BF, // 227: wifi.encryption_algorithm
0x777DC0EB, // 228: iso_struct.r_min
0x7924ABD9, // 229: inverter_sn
0x792A7B79, // 230: io_board.s0_direction
0x7946D888, // 231: i_dc_slow_time
0x79C0A724, // 232: energy.e_ac_total_sum
0x7A67E33B, // 233: can_bus.bms_update_response[1]
0x7A9091EA, // 234: rb485.u_l_grid[1]
0x7AB9B045, // 235: energy.e_dc_month[1]
0x7B1F7FBE, // 236: wifi.gateway
0x7C556C7A, // 237: io_board.io2_polarity
0x7C78CBAC, // 238: g_sync.q_ac_sum_lp
0x7DA7D8B6, // 239: db.power_board.version_main
0x7DDE352B, // 240: wifi.sockb_ip
0x7E096024, // 241: energy.e_load_total_sum
0x7F813D73, // 242: fault[3].flt
0x812E5ADD, // 243: energy.e_dc_total_sum[1]
0x81AE960B, // 244: energy.e_dc_month[0]
0x82258C01, // 245: cs_neg[0]
0x82CD1525, // 246: grid_mon[1].u_under.threshold
0x82E3C121, // 247: g_sync.q_ac[1]
0x8320B84C, // 248: io_board.rse_data_delay
0x83A5333A, // 249: nsm.cos_phi_p[0][0]
0x84ABE3D8, // 250: energy.e_grid_feed_year_sum
0x85886E2E, // 251: p_rec_lim[0]
0x867DEF7D, // 252: energy.e_grid_load_day
0x872F380B, // 253: io_board.load_set
0x87E4387A, // 254: current_sensor_max
0x883DE9AB, // 255: g_sync.s_ac_lp[2]
0x887D43C4, // 256: g_sync.i_dr_lp[0]
0x88C9707B, // 257: io_board.rse_table[15]
0x88DEBCFE, // 258: nsm.q_u_max_u_high
0x88F36D45, // 259: io_board.rse_data
0x89EE3EB5, // 260: g_sync.i_dr_eff[0]
0x8A18539B, // 261: g_sync.u_zk_sum_avg
0x8B9FF008, // 262: battery.soc_target
0x8CA00014, // 263: wifi.result
0x8DD1C728, // 264: dc_conv.dc_conv_struct[1].mpp.enable_scan
0x8E41FC47, // 265: iso_struct.Rp
0x8EBF9574, // 266: power_mng.soc_min_island
0x8EC4116E, // 267: display_struct.blink
0x8EF6FBBD, // 268: battery.cells[1]
0x8F0FF9F3, // 269: p_rec_available[1]
0x8FC89B10, // 270: com_service
0x902AFAFB, // 271: battery.temperature
0x905F707B, // 272: rb485.f_wr[2]
0x9061EA7B, // 273: grid_lt.granularity
0x90B53336, // 274: temperature.sink_temp_power_reduction
0x90F123FA, // 275: io_board.io1_usage
0x915CD4A4, // 276: grid_mon[1].f_over.threshold
0x91617C58, // 277: g_sync.p_ac_grid_sum_lp
0x917E3622, // 278: energy.e_ext_year
0x9214A00C, // 279: hw_test.booster_test_index
0x921997EE, // 280: logger.month_egrid_feed_log_ts
0x9247DB99, // 281: logger.minutes_egrid_load_log_ts
0x929394B7, // 282: svnversion_last_known
0x92BC682B, // 283: g_sync.i_dr_eff[2]
0x933F9A24, // 284: grid_mon[0].f_over.time
0x934E64E9, // 285: switch_on_cond.u_max
0x93C0C2E2, // 286: power_mng.bat_calib_reqularity
0x93E6918D, // 287: nsm.f_exit
0x93F976AB, // 288: rb485.u_l_grid[0]
0x9558AD8A, // 289: rb485.f_grid[0]
0x959930BF, // 290: battery.soc
0x96629BB9, // 291: can_bus.bms_update_state
0x9680077F, // 292: nsm.cos_phi_p[2][0]
0x96E32D11, // 293: flash_param.erase_cycles
0x972B3029, // 294: power_mng.stop_discharge_voltage_buffer
0x97997C93, // 295: power_mng.soc_max
0x97E203F9, // 296: power_mng.is_grid
0x97E3A6F2, // 297: power_mng.u_acc_lp
0x98ACC1B8, // 298: io_board.rse_table[4]
0x99396810, // 299: battery.module_sn[1]
0x99EE89CB, // 300: power_mng.power_lim_src_index
0x9A51A23B, // 301: logger.log_rate
0x9A67600D, // 302: p_rec_lim[2]
0x9B92023F, // 303: io_board.rse_table[7]
0x9D785E8C, // 304: battery.bms_software_version
0x9DC927AA, // 305: bat_mng_struct.profile_load
0x9E1A88F5, // 306: dc_conv.dc_conv_struct[0].mpp.fixed_voltage
0xA12BE39C, // 307: energy.e_load_month_sum
0xA12E9B43, // 308: phase_marker
0xA3393749, // 309: io_board.check_start
0xA33D0954, // 310: nsm.q_u_hysteresis
0xA40906BF, // 311: battery.stack_software_version[4]
0xA5341F4A, // 312: energy.e_grid_feed_month_sum
0xA54C4685, // 313: battery.stack_software_version[1]
0xA59C8428, // 314: energy.e_ext_total
0xA60082A9, // 315: logger.minutes_egrid_feed_log_ts
0xA616B022, // 316: battery.soc_target_low
0xA6271C2E, // 317: grid_mon[0].u_over.threshold
0xA7447FC4, // 318: temperature.bat_temp_power_reduction
0xA76AE9CA, // 319: relays.bits_real
0xA7C708EB, // 320: logger.minutes_eload_log_ts
0xA7FA5C5D, // 321: power_mng.u_acc_mix_lp
0xA9033880, // 322: battery.used_energy
0xA95AD038, // 323: grid_mon[0].f_under.threshold
0xAA9AA253, // 324: dc_conv.dc_conv_struct[1].p_dc
0xAACE057A, // 325: io_board.io1_s0_min_duration
0xAC2E2A56, // 326: io_board.rse_table[5]
0xACF7666B, // 327: battery.efficiency
0xAEF76FA1, // 328: power_mng.minimum_discharge_voltage
0xAF64D0FE, // 329: energy.e_dc_year[0]
0xB0041187, // 330: g_sync.u_sg_avg[1]
0xB0307591, // 331: db.power_board.status
0xB0EBE75A, // 332: battery.minimum_discharge_voltage
0xB0FA4D23, // 333: acc_conv.i_charge_max
0xB1EF67CE, // 334: energy.e_ac_total
0xB20D1AD6, // 335: logger.day_egrid_feed_log_ts
0xB221BCFA, // 336: g_sync.p_ac_sc[2]
0xB238942F, // 337: last_successfull_flash_op
0xB298395D, // 338: dc_conv.dc_conv_struct[0].u_sg_lp
0xB2FB9A90, // 339: bat_mng_struct.k_trust
0xB408E40A, // 340: acc_conv.i_acc_lp_slow
0xB45FE275, // 341: p_rec_available[0]
0xB5317B78, // 342: dc_conv.dc_conv_struct[0].p_dc
0xB55BA2CE, // 343: g_sync.u_sg_avg[0]
0xB57B59BD, // 344: battery.ah_capacity
0xB6623608, // 345: power_mng.bat_next_calib_date
0xB76E2B4C, // 346: nsm.cos_phi_const
0xB7B2967F, // 347: energy.e_dc_total_sum[0]
0xB7C85C51, // 348: wifi.use_ethernet
0xB836B50C, // 349: dc_conv.dc_conv_struct[1].rescan_correction
0xB84A38AB, // 350: battery.soc_target_high
0xB84FDCF9, // 351: adc.u_acc
0xB851FA70, // 352: io_board.rse_table[11]
0xB9928C51, // 353: g_sync.p_ac_lp[2]
0xB9A026F9, // 354: energy.e_ext_day
0xBB617E51, // 355: nsm.u_q_u[1]
0xBCA77559, // 356: g_sync.q_ac[2]
0xBCC6F92F, // 357: io_board.home_relay_threshold
0xBD008E29, // 358: power_mng.battery_power_extern
0xBD3A23C3, // 359: power_mng.soc_charge
0xBD55905F, // 360: energy.e_ac_day
0xBD55D796, // 361: energy.e_dc_year[1]
0xBDFE5547, // 362: io_board.rse_table[3]
0xBF9B6042, // 363: svnversion_factory
0xC03462F6, // 364: g_sync.p_ac[2]
0xC0B7C4D2, // 365: db.power_board.afi_t30
0xC0CC81B6, // 366: energy.e_ac_year
0xC0DF2978, // 367: battery.cycles
0xC198B25B, // 368: g_sync.u_zk_p_avg
0xC1D051EC, // 369: display_struct.variate_contrast
0xC24E85D0, // 370: db.core_temp
0xC3352B17, // 371: nsm.rpm
0xC36675D4, // 372: i_ac_max_set
0xC3A3F070, // 373: i_ac_extern_connected
0xC40D5688, // 374: prim_sm.state_source
0xC46E9CA4, // 375: nsm.u_lock_out
0xC55EF32E, // 376: logger.year_egrid_load_log_ts
0xC642B9D6, // 377: acc_conv.i_discharge_max
0xC717D1FB, // 378: iso_struct.Riso
0xC7459513, // 379: power_mng.force_inv_class
0xC7D3B479, // 380: energy.e_load_year
0xC8609C8E, // 381: battery.cells[3]
0xC8BA1729, // 382: battery.stack_software_version[2]
0xC9D76279, // 383: energy.e_dc_day_sum[0]
0xCA6D6472, // 384: logger.day_eload_log_ts
0xCABC44CA, // 385: g_sync.s_ac[0]
0xCB1B3B10, // 386: io_board.io2_s0_min_duration
0xCB9E1E6C, // 387: nsm.Q_const
0xCBDAD315, // 388: logger.minutes_ebat_log_ts
0xCBEC8200, // 389: hw_test.timer2
0xCCB51399, // 390: nsm.q_u_max_u_low
0xCE266F0F, // 391: power_mng.soc_min
0xCF005C54, // 392: prim_sm.phase_3_mode
0xCF053085, // 393: g_sync.u_l_rms[0]
0xD143A391, // 394: can_bus.set_cell_v_t
0xD166D94D, // 395: flash_rtc.time_stamp
0xD197CBE0, // 396: power_mng.stop_charge_current
0xD1DFC969, // 397: power_mng.soc_target_set
0xD3E94E6B, // 398: logger.minutes_temp_bat_log_ts
0xD451EF88, // 399: cs_map[2]
0xD45913EC, // 400: io_board.rse_table[13]
0xD580567B, // 401: nsm.u_lock_in
0xD884AF95, // 402: nsm.pf_desc_grad
0xD9D66B76, // 403: energy.e_grid_load_year_sum
0xD9E721A5, // 404: grid_lt.timeframe
0xD9F9F35B, // 405: acc_conv.state_slow
0xDA207111, // 406: energy.e_grid_load_month_sum
0xDABD323E, // 407: osci_struct.error
0xDB11855B, // 408: dc_conv.dc_conv_struct[0].p_dc_lp
0xDB2D69AE, // 409: g_sync.p_ac_sum_lp
0xDB45ABD0, // 410: dc_conv.dc_conv_struct[0].rescan_correction
0xDC667958, // 411: power_mng.state
0xDCA1CF26, // 412: g_sync.s_ac_sum_lp
0xDCAC0EA9, // 413: g_sync.i_dr_lp[1]
0xDD90A328, // 414: flash_rtc.time_stamp_update
0xDDD1C2D0, // 415: svnversion
0xDE17F021, // 416: energy.e_grid_load_year
0xDF0A735C, // 417: battery.maximum_discharge_current
0xDF6EA121, // 418: bat_mng_struct.profile_pdc
0xE04C3900, // 419: logger.day_eac_log_ts
0xE0E16E63, // 420: cs_map[0]
0xE14B8679, // 421: i_dc_slow_max
0xE271C6D2, // 422: nsm.u_q_u[2]
0xE29C24EB, // 423: logger.minutes_eac3_log_ts
0xE31F8B17, // 424: prim_sm.Uzk_pump_grad[0]
0xE3F4D1DF, // 425: acc_conv.i_max
0xE49BE3ED, // 426: nsm.pf_rise_grad
0xE4DC040A, // 427: logger.month_eext_log_ts
0xE52B89FA, // 428: io_board.home_relay_off_threshold
0xE5FBCC6F, // 429: logger.year_eload_log_ts
0xE63A3529, // 430: flash_result
0xE94C2EFC, // 431: g_sync.q_ac[0]
0xE96F1844, // 432: io_board.s0_external_power
0xE9BBF6E4, // 433: power_mng.amp_hours_measured
0xEAEEB3CA, // 434: energy.e_dc_month_sum[0]
0xEBC62737, // 435: android_description
0xEBF7A4E8, // 436: grid_mon[0].f_over.threshold
0xECABB6CF, // 437: switch_on_cond.test_time
0xEE049B1F, // 438: nsm.pf_hysteresis
0xEEA3F59B, // 439: battery.stack_software_version[5]
0xEF89568B, // 440: grid_mon[0].u_under.time
0xEFF4B537, // 441: energy.e_load_total
0xF09CC4A2, // 442: grid_mon[1].u_over.time
0xF0A03A20, // 443: bat_mng_struct.k
0xF0BE6429, // 444: energy.e_load_month
0xF1342795, // 445: power_mng.stop_discharge_current
0xF168B748, // 446: power_mng.soc_strategy
0xF1FA5BB9, // 447: grid_mon[1].f_under.time
0xF2405AC6, // 448: nsm.p_limit
0xF247BB16, // 449: display_struct.contrast
0xF25591AA, // 450: nsm.cos_phi_p[3][0]
0xF25C339B, // 451: g_sync.u_ptp_rms[2]
0xF28341E2, // 452: logger.month_eac_log_ts
0xF2BE0C9C, // 453: p_buf_available
0xF393B7B0, // 454: power_mng.calib_charge_power
0xF42D4DD0, // 455: io_board.alarm_home_value
0xF473BC5E, // 456: buf_v_control.power_reduction_max_solar_grid
0xF5584F90, // 457: g_sync.p_ac_sc[1]
0xF644DCA7, // 458: bat_mng_struct.k_reserve
0xF6A85818, // 459: nsm.f_entry
0xF76DE445, // 460: logger.minutes_temp_log_ts
0xF79D41D9, // 461: db.temp1
0xF87A2A1E, // 462: dc_conv.last_rescan
0xF8C0D255, // 463: battery.cells[0]
0xF8DECCE6, // 464: wifi.connected_ap_ssid
0xFA7DB323, // 465: io_board.check_s0_result
0xFAE429C5, // 466: rb485.f_grid[1]
0xFB57BA65, // 467: bat_mng_struct.count
0xFBD94C1F, // 468: power_mng.amp_hours
0xFBF3CE97, // 469: energy.e_dc_day[1]
0xFBF6D834, // 470: battery.module_sn[0]
0xFBF8D63C, // 471: energy.e_grid_load_day_sum
0xFC1C614E, // 472: energy.e_ac_month_sum
0xFC724A9E, // 473: energy.e_dc_total[0]
0xFCC39293, // 474: nsm.rpm_lock_in_power
0xFCF4E78D, // 475: logger.day_ea_log_ts
0xFD4F17C4, // 476: grid_mon[1].f_over.time
0xFDB81124, // 477: energy.e_grid_feed_day_sum
0xFE1AA500, // 478: buf_v_control.power_reduction
0xFED51BD2 }; // 479: dc_conv.dc_conv_struct[1].enabled
#define INSERT_VAR_IDS_HERE

/** Variables Sizes */
#define INSERT_VAR_SIZES_HERE
const uint16_t var_size[] = { // this line can be edited
sizeof(rb485.f_grid[2]),
sizeof(grid_mon[0].u_over.time),
sizeof(cs_neg[2]),
sizeof(energy.e_ext_month),
sizeof(hw_test.state),
sizeof(logger.day_egrid_load_log_ts),
sizeof(logger.minutes_ubat_log_ts),
sizeof(battery.charged_amp_hours),
sizeof(wifi.ip),
sizeof(rb485.phase_marker),
sizeof(adc.u_ref_1_5v[0]),
sizeof(net.id),
sizeof(battery.stack_software_version[3]),
sizeof(logger.minutes_ul3_log_ts),
sizeof(g_sync.u_zk_n_avg),
sizeof(p_rec_req[1]),
sizeof(acc_conv.i_acc_lp_fast),
sizeof(wifi.sockb_protocol),
sizeof(energy.e_ext_day_sum),
sizeof(dc_conv.dc_conv_struct[1].p_dc_lp),
sizeof(nsm.u_q_u[3]),
sizeof(battery.status2),
sizeof(logger.day_eb_log_ts),
sizeof(io_board.io1_polarity),
sizeof(flash_rtc.time_stamp_set),
sizeof(io_board.rse_table[0]),
sizeof(energy.e_ext_total_sum),
sizeof(logger.minutes_ub_log_ts),
sizeof(io_board.check_rs485_result),
sizeof(nsm.cos_phi_p[3][1]),
sizeof(nsm.u_q_u[0]),
sizeof(max_phase_shift),
sizeof(energy.e_ac_month),
sizeof(power_mng.battery_power),
sizeof(energy.e_grid_load_month),
sizeof(logger.minutes_temp2_log_ts),
sizeof(battery.cells[4]),
sizeof(g_sync.p_ac[1]),
sizeof(wifi.password),
sizeof(nsm.rpm_lock_out_power),
sizeof(battery.module_sn[5]),
sizeof(battery.bms_sn),
sizeof(adc.u_ref_1_5v[1]),
sizeof(rb485.version_boot),
sizeof(partition[3].last_id),
sizeof(logger.year_egrid_feed_log_ts),
sizeof(p_rec_req[0]),
sizeof(g_sync.p_ac_load_sum_lp),
sizeof(battery.bms_power_version),
sizeof(io_board.check_rse_result),
sizeof(energy.e_grid_load_total_sum),
sizeof(grid_pll[0].f),
sizeof(wifi.dns_address),
sizeof(power_mng.soc_charge_power),
sizeof(logger.minutes_eb_log_ts),
sizeof(battery.maximum_charge_current),
sizeof(switch_on_cond.u_min),
sizeof(logger.minutes_eac1_log_ts),
sizeof(battery.current),
sizeof(energy.e_dc_month_sum[1]),
sizeof(rb485.u_l_grid[2]),
sizeof(flash_rtc.rtc_mcc_quartz_max_diff),
sizeof(fault[1].flt),
sizeof(switch_on_cond.f_min),
sizeof(net.p_ac_ext_sum),
sizeof(g_sync.u_zk_sum_mov_avg),
sizeof(energy.e_dc_day_sum[1]),
sizeof(g_sync.u_l_rms[2]),
sizeof(battery.module_sn[3]),
sizeof(nsm.cos_phi_p[1][0]),
sizeof(energy.e_grid_feed_year),
sizeof(cs_struct.is_tuned),
sizeof(rb485.version_main),
sizeof(g_sync.p_ac_sc[0]),
sizeof(energy.e_grid_feed_total_sum),
sizeof(display_struct.brightness),
sizeof(io_board.rse_table[10]),
sizeof(logger.year_log_ts),
sizeof(energy.e_dc_day[0]),
sizeof(battery.discharged_amp_hours),
sizeof(io_board.home_relay_sw_off_delay),
sizeof(db.power_board.afi_t300),
sizeof(logger.month_ea_log_ts),
sizeof(energy.e_load_day),
sizeof(grid_mon[1].u_under.time),
sizeof(power_mng.bat_empty_full),
sizeof(nsm.cos_phi_p[0][1]),
sizeof(logger.year_eb_log_ts),
sizeof(nsm.startup_grad),
sizeof(prim_sm.island_flag),
sizeof(p_rec_req[2]),
sizeof(power_mng.use_grid_power_enable),
sizeof(fault[0].flt),
sizeof(battery.soh),
sizeof(db.power_board.afi_i60),
sizeof(flash_rtc.flag_time_auto_switch),
sizeof(logger.minutes_eext_log_ts),
sizeof(grid_lt.threshold),
sizeof(g_sync.s_ac_lp[0]),
sizeof(energy.e_ac_day_sum),
sizeof(energy.e_ext_year_sum),
sizeof(net.package),
sizeof(prim_sm.is_thin_layer),
sizeof(bat_mng_struct.profile_pdc_max),
sizeof(rb485.f_wr[0]),
sizeof(fault[2].flt),
sizeof(io_board.rse_table[8]),
sizeof(energy.e_grid_feed_day),
sizeof(grid_mon[0].u_under.threshold),
sizeof(io_board.rse_table[6]),
sizeof(grid_mon[1].f_under.threshold),
sizeof(g_sync.p_acc_lp),
sizeof(g_sync.s_ac_lp[1]),
sizeof(battery.cells[6]),
sizeof(logger.month_eload_log_ts),
sizeof(g_sync.p_ac[0]),
sizeof(rb485.available),
sizeof(nsm.cos_phi_p[1][1]),
sizeof(nsm.pf_delay),
sizeof(flash_state),
sizeof(db.power_board.afi_t60),
sizeof(energy.e_grid_feed_total),
sizeof(can_bus.bms_update_response[0]),
sizeof(flash_param.write_cycles),
sizeof(iso_struct.Rn),
sizeof(g_sync.u_ptp_rms[1]),
sizeof(logger.minutes_ul2_log_ts),
sizeof(g_sync.i_dr_lp[2]),
sizeof(energy.e_dc_year_sum[0]),
sizeof(battery.prog_sn),
sizeof(buf_v_control.power_reduction_max_solar),
sizeof(energy.e_load_day_sum),
sizeof(cs_neg[1]),
sizeof(logger.year_ea_log_ts),
sizeof(nsm.cos_phi_p[2][1]),
sizeof(nsm.startup_grad_after_fault),
sizeof(switch_on_cond.f_max),
sizeof(flash_rtc.rtc_mcc_quartz_ppm_difference),
sizeof(phase_3_mode),
sizeof(g_sync.p_ac_sum),
sizeof(battery.module_sn[4]),
sizeof(logger.year_eext_log_ts),
sizeof(energy.e_load_year_sum),
sizeof(io_board.io2_usage),
sizeof(db.temp2),
sizeof(adc.u_ref_1_5v[3]),
sizeof(logger.minutes_ea_log_ts),
sizeof(logger.minutes_soc_log_ts),
sizeof(logger.minutes_ul1_log_ts),
sizeof(grid_mon[1].u_over.threshold),
sizeof(p_rec_lim[1]),
sizeof(g_sync.u_l_rms[1]),
sizeof(io_board.rse_table[12]),
sizeof(logger.minutes_eac2_log_ts),
sizeof(battery.stored_energy),
sizeof(g_sync.s_ac[2]),
sizeof(wifi.connect_to_wifi),
sizeof(wifi.authentication_method),
sizeof(io_board.rse_table[2]),
sizeof(io_board.check_state),
sizeof(power_mng.maximum_charge_voltage),
sizeof(battery.module_sn[6]),
sizeof(wifi.mask),
sizeof(wifi.mode),
sizeof(power_mng.is_heiphoss),
sizeof(dc_conv.dc_conv_struct[1].u_sg_lp),
sizeof(io_board.io1_s0_imp_per_kwh),
sizeof(db.power_board.afi_t150),
sizeof(p_rec_available[2]),
sizeof(logger.month_egrid_load_log_ts),
sizeof(dc_conv.dc_conv_struct[1].mpp.fixed_voltage),
sizeof(io_board.alarm_home_relay_mode),
sizeof(prim_sm.state),
sizeof(g_sync.p_ac_sc_sum),
sizeof(logger.day_eext_log_ts),
sizeof(g_sync.s_ac[1]),
sizeof(db.power_board.version_boot),
sizeof(dc_conv.start_voltage),
sizeof(battery.cells[5]),
sizeof(energy.e_grid_load_total),
sizeof(g_sync.u_ptp_rms[0]),
sizeof(battery.stack_software_version[0]),
sizeof(dc_conv.dc_conv_struct[0].mpp.enable_scan),
sizeof(g_sync.i_dr_eff[1]),
sizeof(flash_mem),
sizeof(energy.e_grid_feed_month),
sizeof(battery.voltage),
sizeof(io_board.rse_table[14]),
sizeof(logger.minutes_eac_log_ts),
sizeof(energy.e_ac_year_sum),
sizeof(display_struct.display_dir),
sizeof(power_mng.battery_type),
sizeof(io_board.rse_table[9]),
sizeof(io_board.io2_s0_imp_per_kwh),
sizeof(parameter_file),
sizeof(energy.e_dc_total[1]),
sizeof(battery.stack_software_version[6]),
sizeof(can_bus.requested_id),
sizeof(battery.cells[2]),
sizeof(logger.month_eb_log_ts),
sizeof(db.power_board.afi_i30),
sizeof(modbus.address),
sizeof(io_board.rse_table[1]),
sizeof(i_dc_max),
sizeof(energy.e_dc_year_sum[1]),
sizeof(cs_map[1]),
sizeof(g_sync.p_ac_lp[1]),
sizeof(battery.maximum_charge_voltage),
sizeof(logger.error_log_time_stamp),
sizeof(db.power_board.afi_i150),
sizeof(rb485.f_wr[1]),
sizeof(energy.e_ext_month_sum),
sizeof(dc_conv.dc_conv_struct[0].enabled),
sizeof(battery.bat_status),
sizeof(logger.year_eac_log_ts),
sizeof(grid_mon[0].f_under.time),
sizeof(adc.u_ref_1_5v[2]),
sizeof(battery.status),
sizeof(g_sync.p_ac_lp[0]),
sizeof(nsm.apm),
sizeof(battery.inv_cmd),
sizeof(logger.minutes_ua_log_ts),
sizeof(flash_rtc.time_stamp_factory),
sizeof(battery.module_sn[2]),
sizeof(db.power_board.afi_i300),
sizeof(io_board.home_relay_sw_on_delay),
sizeof(logger.minutes_soc_targ_log_ts),
sizeof(wifi.encryption_algorithm),
sizeof(iso_struct.r_min),
sizeof(inverter_sn),
sizeof(io_board.s0_direction),
sizeof(i_dc_slow_time),
sizeof(energy.e_ac_total_sum),
sizeof(can_bus.bms_update_response[1]),
sizeof(rb485.u_l_grid[1]),
sizeof(energy.e_dc_month[1]),
sizeof(wifi.gateway),
sizeof(io_board.io2_polarity),
sizeof(g_sync.q_ac_sum_lp),
sizeof(db.power_board.version_main),
sizeof(wifi.sockb_ip),
sizeof(energy.e_load_total_sum),
sizeof(fault[3].flt),
sizeof(energy.e_dc_total_sum[1]),
sizeof(energy.e_dc_month[0]),
sizeof(cs_neg[0]),
sizeof(grid_mon[1].u_under.threshold),
sizeof(g_sync.q_ac[1]),
sizeof(io_board.rse_data_delay),
sizeof(nsm.cos_phi_p[0][0]),
sizeof(energy.e_grid_feed_year_sum),
sizeof(p_rec_lim[0]),
sizeof(energy.e_grid_load_day),
sizeof(io_board.load_set),
sizeof(current_sensor_max),
sizeof(g_sync.s_ac_lp[2]),
sizeof(g_sync.i_dr_lp[0]),
sizeof(io_board.rse_table[15]),
sizeof(nsm.q_u_max_u_high),
sizeof(io_board.rse_data),
sizeof(g_sync.i_dr_eff[0]),
sizeof(g_sync.u_zk_sum_avg),
sizeof(battery.soc_target),
sizeof(wifi.result),
sizeof(dc_conv.dc_conv_struct[1].mpp.enable_scan),
sizeof(iso_struct.Rp),
sizeof(power_mng.soc_min_island),
sizeof(display_struct.blink),
sizeof(battery.cells[1]),
sizeof(p_rec_available[1]),
sizeof(com_service),
sizeof(battery.temperature),
sizeof(rb485.f_wr[2]),
sizeof(grid_lt.granularity),
sizeof(temperature.sink_temp_power_reduction),
sizeof(io_board.io1_usage),
sizeof(grid_mon[1].f_over.threshold),
sizeof(g_sync.p_ac_grid_sum_lp),
sizeof(energy.e_ext_year),
sizeof(hw_test.booster_test_index),
sizeof(logger.month_egrid_feed_log_ts),
sizeof(logger.minutes_egrid_load_log_ts),
sizeof(svnversion_last_known),
sizeof(g_sync.i_dr_eff[2]),
sizeof(grid_mon[0].f_over.time),
sizeof(switch_on_cond.u_max),
sizeof(power_mng.bat_calib_reqularity),
sizeof(nsm.f_exit),
sizeof(rb485.u_l_grid[0]),
sizeof(rb485.f_grid[0]),
sizeof(battery.soc),
sizeof(can_bus.bms_update_state),
sizeof(nsm.cos_phi_p[2][0]),
sizeof(flash_param.erase_cycles),
sizeof(power_mng.stop_discharge_voltage_buffer),
sizeof(power_mng.soc_max),
sizeof(power_mng.is_grid),
sizeof(power_mng.u_acc_lp),
sizeof(io_board.rse_table[4]),
sizeof(battery.module_sn[1]),
sizeof(power_mng.power_lim_src_index),
sizeof(logger.log_rate),
sizeof(p_rec_lim[2]),
sizeof(io_board.rse_table[7]),
sizeof(battery.bms_software_version),
sizeof(bat_mng_struct.profile_load),
sizeof(dc_conv.dc_conv_struct[0].mpp.fixed_voltage),
sizeof(energy.e_load_month_sum),
sizeof(phase_marker),
sizeof(io_board.check_start),
sizeof(nsm.q_u_hysteresis),
sizeof(battery.stack_software_version[4]),
sizeof(energy.e_grid_feed_month_sum),
sizeof(battery.stack_software_version[1]),
sizeof(energy.e_ext_total),
sizeof(logger.minutes_egrid_feed_log_ts),
sizeof(battery.soc_target_low),
sizeof(grid_mon[0].u_over.threshold),
sizeof(temperature.bat_temp_power_reduction),
sizeof(relays.bits_real),
sizeof(logger.minutes_eload_log_ts),
sizeof(power_mng.u_acc_mix_lp),
sizeof(battery.used_energy),
sizeof(grid_mon[0].f_under.threshold),
sizeof(dc_conv.dc_conv_struct[1].p_dc),
sizeof(io_board.io1_s0_min_duration),
sizeof(io_board.rse_table[5]),
sizeof(battery.efficiency),
sizeof(power_mng.minimum_discharge_voltage),
sizeof(energy.e_dc_year[0]),
sizeof(g_sync.u_sg_avg[1]),
sizeof(db.power_board.status),
sizeof(battery.minimum_discharge_voltage),
sizeof(acc_conv.i_charge_max),
sizeof(energy.e_ac_total),
sizeof(logger.day_egrid_feed_log_ts),
sizeof(g_sync.p_ac_sc[2]),
sizeof(last_successfull_flash_op),
sizeof(dc_conv.dc_conv_struct[0].u_sg_lp),
sizeof(bat_mng_struct.k_trust),
sizeof(acc_conv.i_acc_lp_slow),
sizeof(p_rec_available[0]),
sizeof(dc_conv.dc_conv_struct[0].p_dc),
sizeof(g_sync.u_sg_avg[0]),
sizeof(battery.ah_capacity),
sizeof(power_mng.bat_next_calib_date),
sizeof(nsm.cos_phi_const),
sizeof(energy.e_dc_total_sum[0]),
sizeof(wifi.use_ethernet),
sizeof(dc_conv.dc_conv_struct[1].rescan_correction),
sizeof(battery.soc_target_high),
sizeof(adc.u_acc),
sizeof(io_board.rse_table[11]),
sizeof(g_sync.p_ac_lp[2]),
sizeof(energy.e_ext_day),
sizeof(nsm.u_q_u[1]),
sizeof(g_sync.q_ac[2]),
sizeof(io_board.home_relay_threshold),
sizeof(power_mng.battery_power_extern),
sizeof(power_mng.soc_charge),
sizeof(energy.e_ac_day),
sizeof(energy.e_dc_year[1]),
sizeof(io_board.rse_table[3]),
sizeof(svnversion_factory),
sizeof(g_sync.p_ac[2]),
sizeof(db.power_board.afi_t30),
sizeof(energy.e_ac_year),
sizeof(battery.cycles),
sizeof(g_sync.u_zk_p_avg),
sizeof(display_struct.variate_contrast),
sizeof(db.core_temp),
sizeof(nsm.rpm),
sizeof(i_ac_max_set),
sizeof(i_ac_extern_connected),
sizeof(prim_sm.state_source),
sizeof(nsm.u_lock_out),
sizeof(logger.year_egrid_load_log_ts),
sizeof(acc_conv.i_discharge_max),
sizeof(iso_struct.Riso),
sizeof(power_mng.force_inv_class),
sizeof(energy.e_load_year),
sizeof(battery.cells[3]),
sizeof(battery.stack_software_version[2]),
sizeof(energy.e_dc_day_sum[0]),
sizeof(logger.day_eload_log_ts),
sizeof(g_sync.s_ac[0]),
sizeof(io_board.io2_s0_min_duration),
sizeof(nsm.Q_const),
sizeof(logger.minutes_ebat_log_ts),
sizeof(hw_test.timer2),
sizeof(nsm.q_u_max_u_low),
sizeof(power_mng.soc_min),
sizeof(prim_sm.phase_3_mode),
sizeof(g_sync.u_l_rms[0]),
sizeof(can_bus.set_cell_v_t),
sizeof(flash_rtc.time_stamp),
sizeof(power_mng.stop_charge_current),
sizeof(power_mng.soc_target_set),
sizeof(logger.minutes_temp_bat_log_ts),
sizeof(cs_map[2]),
sizeof(io_board.rse_table[13]),
sizeof(nsm.u_lock_in),
sizeof(nsm.pf_desc_grad),
sizeof(energy.e_grid_load_year_sum),
sizeof(grid_lt.timeframe),
sizeof(acc_conv.state_slow),
sizeof(energy.e_grid_load_month_sum),
sizeof(osci_struct.error),
sizeof(dc_conv.dc_conv_struct[0].p_dc_lp),
sizeof(g_sync.p_ac_sum_lp),
sizeof(dc_conv.dc_conv_struct[0].rescan_correction),
sizeof(power_mng.state),
sizeof(g_sync.s_ac_sum_lp),
sizeof(g_sync.i_dr_lp[1]),
sizeof(flash_rtc.time_stamp_update),
sizeof(svnversion),
sizeof(energy.e_grid_load_year),
sizeof(battery.maximum_discharge_current),
sizeof(bat_mng_struct.profile_pdc),
sizeof(logger.day_eac_log_ts),
sizeof(cs_map[0]),
sizeof(i_dc_slow_max),
sizeof(nsm.u_q_u[2]),
sizeof(logger.minutes_eac3_log_ts),
sizeof(prim_sm.Uzk_pump_grad[0]),
sizeof(acc_conv.i_max),
sizeof(nsm.pf_rise_grad),
sizeof(logger.month_eext_log_ts),
sizeof(io_board.home_relay_off_threshold),
sizeof(logger.year_eload_log_ts),
sizeof(flash_result),
sizeof(g_sync.q_ac[0]),
sizeof(io_board.s0_external_power),
sizeof(power_mng.amp_hours_measured),
sizeof(energy.e_dc_month_sum[0]),
sizeof(android_description),
sizeof(grid_mon[0].f_over.threshold),
sizeof(switch_on_cond.test_time),
sizeof(nsm.pf_hysteresis),
sizeof(battery.stack_software_version[5]),
sizeof(grid_mon[0].u_under.time),
sizeof(energy.e_load_total),
sizeof(grid_mon[1].u_over.time),
sizeof(bat_mng_struct.k),
sizeof(energy.e_load_month),
sizeof(power_mng.stop_discharge_current),
sizeof(power_mng.soc_strategy),
sizeof(grid_mon[1].f_under.time),
sizeof(nsm.p_limit),
sizeof(display_struct.contrast),
sizeof(nsm.cos_phi_p[3][0]),
sizeof(g_sync.u_ptp_rms[2]),
sizeof(logger.month_eac_log_ts),
sizeof(p_buf_available),
sizeof(power_mng.calib_charge_power),
sizeof(io_board.alarm_home_value),
sizeof(buf_v_control.power_reduction_max_solar_grid),
sizeof(g_sync.p_ac_sc[1]),
sizeof(bat_mng_struct.k_reserve),
sizeof(nsm.f_entry),
sizeof(logger.minutes_temp_log_ts),
sizeof(db.temp1),
sizeof(dc_conv.last_rescan),
sizeof(battery.cells[0]),
sizeof(wifi.connected_ap_ssid),
sizeof(io_board.check_s0_result),
sizeof(rb485.f_grid[1]),
sizeof(bat_mng_struct.count),
sizeof(power_mng.amp_hours),
sizeof(energy.e_dc_day[1]),
sizeof(battery.module_sn[0]),
sizeof(energy.e_grid_load_day_sum),
sizeof(energy.e_ac_month_sum),
sizeof(energy.e_dc_total[0]),
sizeof(nsm.rpm_lock_in_power),
sizeof(logger.day_ea_log_ts),
sizeof(grid_mon[1].f_over.time),
sizeof(energy.e_grid_feed_day_sum),
sizeof(buf_v_control.power_reduction),
sizeof(dc_conv.dc_conv_struct[1].enabled) };
#define INSERT_VAR_SIZES_HERE

/** Number of parameters which can be saved in FLASH. */
#define COM_FLASH_PARAMETERS 480

uint16_t com_flash_parameters = COM_FLASH_PARAMETERS;

#define INSERT_VAR_ADDRESSES_HERE
const uint32_t var_addr[] = { // this line can be edited
(uint32_t)&rb485.f_grid[2],
(uint32_t)&grid_mon[0].u_over.time,
(uint32_t)&cs_neg[2],
(uint32_t)&energy.e_ext_month,
(uint32_t)&hw_test.state,
(uint32_t)&logger.day_egrid_load_log_ts,
(uint32_t)&logger.minutes_ubat_log_ts,
(uint32_t)&battery.charged_amp_hours,
(uint32_t)&wifi.ip,
(uint32_t)&rb485.phase_marker,
(uint32_t)&adc.u_ref_1_5v[0],
(uint32_t)&net.id,
(uint32_t)&battery.stack_software_version[3],
(uint32_t)&logger.minutes_ul3_log_ts,
(uint32_t)&g_sync.u_zk_n_avg,
(uint32_t)&p_rec_req[1],
(uint32_t)&acc_conv.i_acc_lp_fast,
(uint32_t)&wifi.sockb_protocol,
(uint32_t)&energy.e_ext_day_sum,
(uint32_t)&dc_conv.dc_conv_struct[1].p_dc_lp,
(uint32_t)&nsm.u_q_u[3],
(uint32_t)&battery.status2,
(uint32_t)&logger.day_eb_log_ts,
(uint32_t)&io_board.io1_polarity,
(uint32_t)&flash_rtc.time_stamp_set,
(uint32_t)&io_board.rse_table[0],
(uint32_t)&energy.e_ext_total_sum,
(uint32_t)&logger.minutes_ub_log_ts,
(uint32_t)&io_board.check_rs485_result,
(uint32_t)&nsm.cos_phi_p[3][1],
(uint32_t)&nsm.u_q_u[0],
(uint32_t)&max_phase_shift,
(uint32_t)&energy.e_ac_month,
(uint32_t)&power_mng.battery_power,
(uint32_t)&energy.e_grid_load_month,
(uint32_t)&logger.minutes_temp2_log_ts,
(uint32_t)&battery.cells[4],
(uint32_t)&g_sync.p_ac[1],
(uint32_t)&wifi.password,
(uint32_t)&nsm.rpm_lock_out_power,
(uint32_t)&battery.module_sn[5],
(uint32_t)&battery.bms_sn,
(uint32_t)&adc.u_ref_1_5v[1],
(uint32_t)&rb485.version_boot,
(uint32_t)&partition[3].last_id,
(uint32_t)&logger.year_egrid_feed_log_ts,
(uint32_t)&p_rec_req[0],
(uint32_t)&g_sync.p_ac_load_sum_lp,
(uint32_t)&battery.bms_power_version,
(uint32_t)&io_board.check_rse_result,
(uint32_t)&energy.e_grid_load_total_sum,
(uint32_t)&grid_pll[0].f,
(uint32_t)&wifi.dns_address,
(uint32_t)&power_mng.soc_charge_power,
(uint32_t)&logger.minutes_eb_log_ts,
(uint32_t)&battery.maximum_charge_current,
(uint32_t)&switch_on_cond.u_min,
(uint32_t)&logger.minutes_eac1_log_ts,
(uint32_t)&battery.current,
(uint32_t)&energy.e_dc_month_sum[1],
(uint32_t)&rb485.u_l_grid[2],
(uint32_t)&flash_rtc.rtc_mcc_quartz_max_diff,
(uint32_t)&fault[1].flt,
(uint32_t)&switch_on_cond.f_min,
(uint32_t)&net.p_ac_ext_sum,
(uint32_t)&g_sync.u_zk_sum_mov_avg,
(uint32_t)&energy.e_dc_day_sum[1],
(uint32_t)&g_sync.u_l_rms[2],
(uint32_t)&battery.module_sn[3],
(uint32_t)&nsm.cos_phi_p[1][0],
(uint32_t)&energy.e_grid_feed_year,
(uint32_t)&cs_struct.is_tuned,
(uint32_t)&rb485.version_main,
(uint32_t)&g_sync.p_ac_sc[0],
(uint32_t)&energy.e_grid_feed_total_sum,
(uint32_t)&display_struct.brightness,
(uint32_t)&io_board.rse_table[10],
(uint32_t)&logger.year_log_ts,
(uint32_t)&energy.e_dc_day[0],
(uint32_t)&battery.discharged_amp_hours,
(uint32_t)&io_board.home_relay_sw_off_delay,
(uint32_t)&db.power_board.afi_t300,
(uint32_t)&logger.month_ea_log_ts,
(uint32_t)&energy.e_load_day,
(uint32_t)&grid_mon[1].u_under.time,
(uint32_t)&power_mng.bat_empty_full,
(uint32_t)&nsm.cos_phi_p[0][1],
(uint32_t)&logger.year_eb_log_ts,
(uint32_t)&nsm.startup_grad,
(uint32_t)&prim_sm.island_flag,
(uint32_t)&p_rec_req[2],
(uint32_t)&power_mng.use_grid_power_enable,
(uint32_t)&fault[0].flt,
(uint32_t)&battery.soh,
(uint32_t)&db.power_board.afi_i60,
(uint32_t)&flash_rtc.flag_time_auto_switch,
(uint32_t)&logger.minutes_eext_log_ts,
(uint32_t)&grid_lt.threshold,
(uint32_t)&g_sync.s_ac_lp[0],
(uint32_t)&energy.e_ac_day_sum,
(uint32_t)&energy.e_ext_year_sum,
(uint32_t)&net.package,
(uint32_t)&prim_sm.is_thin_layer,
(uint32_t)&bat_mng_struct.profile_pdc_max,
(uint32_t)&rb485.f_wr[0],
(uint32_t)&fault[2].flt,
(uint32_t)&io_board.rse_table[8],
(uint32_t)&energy.e_grid_feed_day,
(uint32_t)&grid_mon[0].u_under.threshold,
(uint32_t)&io_board.rse_table[6],
(uint32_t)&grid_mon[1].f_under.threshold,
(uint32_t)&g_sync.p_acc_lp,
(uint32_t)&g_sync.s_ac_lp[1],
(uint32_t)&battery.cells[6],
(uint32_t)&logger.month_eload_log_ts,
(uint32_t)&g_sync.p_ac[0],
(uint32_t)&rb485.available,
(uint32_t)&nsm.cos_phi_p[1][1],
(uint32_t)&nsm.pf_delay,
(uint32_t)&flash_state,
(uint32_t)&db.power_board.afi_t60,
(uint32_t)&energy.e_grid_feed_total,
(uint32_t)&can_bus.bms_update_response[0],
(uint32_t)&flash_param.write_cycles,
(uint32_t)&iso_struct.Rn,
(uint32_t)&g_sync.u_ptp_rms[1],
(uint32_t)&logger.minutes_ul2_log_ts,
(uint32_t)&g_sync.i_dr_lp[2],
(uint32_t)&energy.e_dc_year_sum[0],
(uint32_t)&battery.prog_sn,
(uint32_t)&buf_v_control.power_reduction_max_solar,
(uint32_t)&energy.e_load_day_sum,
(uint32_t)&cs_neg[1],
(uint32_t)&logger.year_ea_log_ts,
(uint32_t)&nsm.cos_phi_p[2][1],
(uint32_t)&nsm.startup_grad_after_fault,
(uint32_t)&switch_on_cond.f_max,
(uint32_t)&flash_rtc.rtc_mcc_quartz_ppm_difference,
(uint32_t)&phase_3_mode,
(uint32_t)&g_sync.p_ac_sum,
(uint32_t)&battery.module_sn[4],
(uint32_t)&logger.year_eext_log_ts,
(uint32_t)&energy.e_load_year_sum,
(uint32_t)&io_board.io2_usage,
(uint32_t)&db.temp2,
(uint32_t)&adc.u_ref_1_5v[3],
(uint32_t)&logger.minutes_ea_log_ts,
(uint32_t)&logger.minutes_soc_log_ts,
(uint32_t)&logger.minutes_ul1_log_ts,
(uint32_t)&grid_mon[1].u_over.threshold,
(uint32_t)&p_rec_lim[1],
(uint32_t)&g_sync.u_l_rms[1],
(uint32_t)&io_board.rse_table[12],
(uint32_t)&logger.minutes_eac2_log_ts,
(uint32_t)&battery.stored_energy,
(uint32_t)&g_sync.s_ac[2],
(uint32_t)&wifi.connect_to_wifi,
(uint32_t)&wifi.authentication_method,
(uint32_t)&io_board.rse_table[2],
(uint32_t)&io_board.check_state,
(uint32_t)&power_mng.maximum_charge_voltage,
(uint32_t)&battery.module_sn[6],
(uint32_t)&wifi.mask,
(uint32_t)&wifi.mode,
(uint32_t)&power_mng.is_heiphoss,
(uint32_t)&dc_conv.dc_conv_struct[1].u_sg_lp,
(uint32_t)&io_board.io1_s0_imp_per_kwh,
(uint32_t)&db.power_board.afi_t150,
(uint32_t)&p_rec_available[2],
(uint32_t)&logger.month_egrid_load_log_ts,
(uint32_t)&dc_conv.dc_conv_struct[1].mpp.fixed_voltage,
(uint32_t)&io_board.alarm_home_relay_mode,
(uint32_t)&prim_sm.state,
(uint32_t)&g_sync.p_ac_sc_sum,
(uint32_t)&logger.day_eext_log_ts,
(uint32_t)&g_sync.s_ac[1],
(uint32_t)&db.power_board.version_boot,
(uint32_t)&dc_conv.start_voltage,
(uint32_t)&battery.cells[5],
(uint32_t)&energy.e_grid_load_total,
(uint32_t)&g_sync.u_ptp_rms[0],
(uint32_t)&battery.stack_software_version[0],
(uint32_t)&dc_conv.dc_conv_struct[0].mpp.enable_scan,
(uint32_t)&g_sync.i_dr_eff[1],
(uint32_t)&flash_mem,
(uint32_t)&energy.e_grid_feed_month,
(uint32_t)&battery.voltage,
(uint32_t)&io_board.rse_table[14],
(uint32_t)&logger.minutes_eac_log_ts,
(uint32_t)&energy.e_ac_year_sum,
(uint32_t)&display_struct.display_dir,
(uint32_t)&power_mng.battery_type,
(uint32_t)&io_board.rse_table[9],
(uint32_t)&io_board.io2_s0_imp_per_kwh,
(uint32_t)&parameter_file,
(uint32_t)&energy.e_dc_total[1],
(uint32_t)&battery.stack_software_version[6],
(uint32_t)&can_bus.requested_id,
(uint32_t)&battery.cells[2],
(uint32_t)&logger.month_eb_log_ts,
(uint32_t)&db.power_board.afi_i30,
(uint32_t)&modbus.address,
(uint32_t)&io_board.rse_table[1],
(uint32_t)&i_dc_max,
(uint32_t)&energy.e_dc_year_sum[1],
(uint32_t)&cs_map[1],
(uint32_t)&g_sync.p_ac_lp[1],
(uint32_t)&battery.maximum_charge_voltage,
(uint32_t)&logger.error_log_time_stamp,
(uint32_t)&db.power_board.afi_i150,
(uint32_t)&rb485.f_wr[1],
(uint32_t)&energy.e_ext_month_sum,
(uint32_t)&dc_conv.dc_conv_struct[0].enabled,
(uint32_t)&battery.bat_status,
(uint32_t)&logger.year_eac_log_ts,
(uint32_t)&grid_mon[0].f_under.time,
(uint32_t)&adc.u_ref_1_5v[2],
(uint32_t)&battery.status,
(uint32_t)&g_sync.p_ac_lp[0],
(uint32_t)&nsm.apm,
(uint32_t)&battery.inv_cmd,
(uint32_t)&logger.minutes_ua_log_ts,
(uint32_t)&flash_rtc.time_stamp_factory,
(uint32_t)&battery.module_sn[2],
(uint32_t)&db.power_board.afi_i300,
(uint32_t)&io_board.home_relay_sw_on_delay,
(uint32_t)&logger.minutes_soc_targ_log_ts,
(uint32_t)&wifi.encryption_algorithm,
(uint32_t)&iso_struct.r_min,
(uint32_t)&inverter_sn,
(uint32_t)&io_board.s0_direction,
(uint32_t)&i_dc_slow_time,
(uint32_t)&energy.e_ac_total_sum,
(uint32_t)&can_bus.bms_update_response[1],
(uint32_t)&rb485.u_l_grid[1],
(uint32_t)&energy.e_dc_month[1],
(uint32_t)&wifi.gateway,
(uint32_t)&io_board.io2_polarity,
(uint32_t)&g_sync.q_ac_sum_lp,
(uint32_t)&db.power_board.version_main,
(uint32_t)&wifi.sockb_ip,
(uint32_t)&energy.e_load_total_sum,
(uint32_t)&fault[3].flt,
(uint32_t)&energy.e_dc_total_sum[1],
(uint32_t)&energy.e_dc_month[0],
(uint32_t)&cs_neg[0],
(uint32_t)&grid_mon[1].u_under.threshold,
(uint32_t)&g_sync.q_ac[1],
(uint32_t)&io_board.rse_data_delay,
(uint32_t)&nsm.cos_phi_p[0][0],
(uint32_t)&energy.e_grid_feed_year_sum,
(uint32_t)&p_rec_lim[0],
(uint32_t)&energy.e_grid_load_day,
(uint32_t)&io_board.load_set,
(uint32_t)&current_sensor_max,
(uint32_t)&g_sync.s_ac_lp[2],
(uint32_t)&g_sync.i_dr_lp[0],
(uint32_t)&io_board.rse_table[15],
(uint32_t)&nsm.q_u_max_u_high,
(uint32_t)&io_board.rse_data,
(uint32_t)&g_sync.i_dr_eff[0],
(uint32_t)&g_sync.u_zk_sum_avg,
(uint32_t)&battery.soc_target,
(uint32_t)&wifi.result,
(uint32_t)&dc_conv.dc_conv_struct[1].mpp.enable_scan,
(uint32_t)&iso_struct.Rp,
(uint32_t)&power_mng.soc_min_island,
(uint32_t)&display_struct.blink,
(uint32_t)&battery.cells[1],
(uint32_t)&p_rec_available[1],
(uint32_t)&com_service,
(uint32_t)&battery.temperature,
(uint32_t)&rb485.f_wr[2],
(uint32_t)&grid_lt.granularity,
(uint32_t)&temperature.sink_temp_power_reduction,
(uint32_t)&io_board.io1_usage,
(uint32_t)&grid_mon[1].f_over.threshold,
(uint32_t)&g_sync.p_ac_grid_sum_lp,
(uint32_t)&energy.e_ext_year,
(uint32_t)&hw_test.booster_test_index,
(uint32_t)&logger.month_egrid_feed_log_ts,
(uint32_t)&logger.minutes_egrid_load_log_ts,
(uint32_t)&svnversion_last_known,
(uint32_t)&g_sync.i_dr_eff[2],
(uint32_t)&grid_mon[0].f_over.time,
(uint32_t)&switch_on_cond.u_max,
(uint32_t)&power_mng.bat_calib_reqularity,
(uint32_t)&nsm.f_exit,
(uint32_t)&rb485.u_l_grid[0],
(uint32_t)&rb485.f_grid[0],
(uint32_t)&battery.soc,
(uint32_t)&can_bus.bms_update_state,
(uint32_t)&nsm.cos_phi_p[2][0],
(uint32_t)&flash_param.erase_cycles,
(uint32_t)&power_mng.stop_discharge_voltage_buffer,
(uint32_t)&power_mng.soc_max,
(uint32_t)&power_mng.is_grid,
(uint32_t)&power_mng.u_acc_lp,
(uint32_t)&io_board.rse_table[4],
(uint32_t)&battery.module_sn[1],
(uint32_t)&power_mng.power_lim_src_index,
(uint32_t)&logger.log_rate,
(uint32_t)&p_rec_lim[2],
(uint32_t)&io_board.rse_table[7],
(uint32_t)&battery.bms_software_version,
(uint32_t)&bat_mng_struct.profile_load,
(uint32_t)&dc_conv.dc_conv_struct[0].mpp.fixed_voltage,
(uint32_t)&energy.e_load_month_sum,
(uint32_t)&phase_marker,
(uint32_t)&io_board.check_start,
(uint32_t)&nsm.q_u_hysteresis,
(uint32_t)&battery.stack_software_version[4],
(uint32_t)&energy.e_grid_feed_month_sum,
(uint32_t)&battery.stack_software_version[1],
(uint32_t)&energy.e_ext_total,
(uint32_t)&logger.minutes_egrid_feed_log_ts,
(uint32_t)&battery.soc_target_low,
(uint32_t)&grid_mon[0].u_over.threshold,
(uint32_t)&temperature.bat_temp_power_reduction,
(uint32_t)&relays.bits_real,
(uint32_t)&logger.minutes_eload_log_ts,
(uint32_t)&power_mng.u_acc_mix_lp,
(uint32_t)&battery.used_energy,
(uint32_t)&grid_mon[0].f_under.threshold,
(uint32_t)&dc_conv.dc_conv_struct[1].p_dc,
(uint32_t)&io_board.io1_s0_min_duration,
(uint32_t)&io_board.rse_table[5],
(uint32_t)&battery.efficiency,
(uint32_t)&power_mng.minimum_discharge_voltage,
(uint32_t)&energy.e_dc_year[0],
(uint32_t)&g_sync.u_sg_avg[1],
(uint32_t)&db.power_board.status,
(uint32_t)&battery.minimum_discharge_voltage,
(uint32_t)&acc_conv.i_charge_max,
(uint32_t)&energy.e_ac_total,
(uint32_t)&logger.day_egrid_feed_log_ts,
(uint32_t)&g_sync.p_ac_sc[2],
(uint32_t)&last_successfull_flash_op,
(uint32_t)&dc_conv.dc_conv_struct[0].u_sg_lp,
(uint32_t)&bat_mng_struct.k_trust,
(uint32_t)&acc_conv.i_acc_lp_slow,
(uint32_t)&p_rec_available[0],
(uint32_t)&dc_conv.dc_conv_struct[0].p_dc,
(uint32_t)&g_sync.u_sg_avg[0],
(uint32_t)&battery.ah_capacity,
(uint32_t)&power_mng.bat_next_calib_date,
(uint32_t)&nsm.cos_phi_const,
(uint32_t)&energy.e_dc_total_sum[0],
(uint32_t)&wifi.use_ethernet,
(uint32_t)&dc_conv.dc_conv_struct[1].rescan_correction,
(uint32_t)&battery.soc_target_high,
(uint32_t)&adc.u_acc,
(uint32_t)&io_board.rse_table[11],
(uint32_t)&g_sync.p_ac_lp[2],
(uint32_t)&energy.e_ext_day,
(uint32_t)&nsm.u_q_u[1],
(uint32_t)&g_sync.q_ac[2],
(uint32_t)&io_board.home_relay_threshold,
(uint32_t)&power_mng.battery_power_extern,
(uint32_t)&power_mng.soc_charge,
(uint32_t)&energy.e_ac_day,
(uint32_t)&energy.e_dc_year[1],
(uint32_t)&io_board.rse_table[3],
(uint32_t)&svnversion_factory,
(uint32_t)&g_sync.p_ac[2],
(uint32_t)&db.power_board.afi_t30,
(uint32_t)&energy.e_ac_year,
(uint32_t)&battery.cycles,
(uint32_t)&g_sync.u_zk_p_avg,
(uint32_t)&display_struct.variate_contrast,
(uint32_t)&db.core_temp,
(uint32_t)&nsm.rpm,
(uint32_t)&i_ac_max_set,
(uint32_t)&i_ac_extern_connected,
(uint32_t)&prim_sm.state_source,
(uint32_t)&nsm.u_lock_out,
(uint32_t)&logger.year_egrid_load_log_ts,
(uint32_t)&acc_conv.i_discharge_max,
(uint32_t)&iso_struct.Riso,
(uint32_t)&power_mng.force_inv_class,
(uint32_t)&energy.e_load_year,
(uint32_t)&battery.cells[3],
(uint32_t)&battery.stack_software_version[2],
(uint32_t)&energy.e_dc_day_sum[0],
(uint32_t)&logger.day_eload_log_ts,
(uint32_t)&g_sync.s_ac[0],
(uint32_t)&io_board.io2_s0_min_duration,
(uint32_t)&nsm.Q_const,
(uint32_t)&logger.minutes_ebat_log_ts,
(uint32_t)&hw_test.timer2,
(uint32_t)&nsm.q_u_max_u_low,
(uint32_t)&power_mng.soc_min,
(uint32_t)&prim_sm.phase_3_mode,
(uint32_t)&g_sync.u_l_rms[0],
(uint32_t)&can_bus.set_cell_v_t,
(uint32_t)&flash_rtc.time_stamp,
(uint32_t)&power_mng.stop_charge_current,
(uint32_t)&power_mng.soc_target_set,
(uint32_t)&logger.minutes_temp_bat_log_ts,
(uint32_t)&cs_map[2],
(uint32_t)&io_board.rse_table[13],
(uint32_t)&nsm.u_lock_in,
(uint32_t)&nsm.pf_desc_grad,
(uint32_t)&energy.e_grid_load_year_sum,
(uint32_t)&grid_lt.timeframe,
(uint32_t)&acc_conv.state_slow,
(uint32_t)&energy.e_grid_load_month_sum,
(uint32_t)&osci_struct.error,
(uint32_t)&dc_conv.dc_conv_struct[0].p_dc_lp,
(uint32_t)&g_sync.p_ac_sum_lp,
(uint32_t)&dc_conv.dc_conv_struct[0].rescan_correction,
(uint32_t)&power_mng.state,
(uint32_t)&g_sync.s_ac_sum_lp,
(uint32_t)&g_sync.i_dr_lp[1],
(uint32_t)&flash_rtc.time_stamp_update,
(uint32_t)&svnversion,
(uint32_t)&energy.e_grid_load_year,
(uint32_t)&battery.maximum_discharge_current,
(uint32_t)&bat_mng_struct.profile_pdc,
(uint32_t)&logger.day_eac_log_ts,
(uint32_t)&cs_map[0],
(uint32_t)&i_dc_slow_max,
(uint32_t)&nsm.u_q_u[2],
(uint32_t)&logger.minutes_eac3_log_ts,
(uint32_t)&prim_sm.Uzk_pump_grad[0],
(uint32_t)&acc_conv.i_max,
(uint32_t)&nsm.pf_rise_grad,
(uint32_t)&logger.month_eext_log_ts,
(uint32_t)&io_board.home_relay_off_threshold,
(uint32_t)&logger.year_eload_log_ts,
(uint32_t)&flash_result,
(uint32_t)&g_sync.q_ac[0],
(uint32_t)&io_board.s0_external_power,
(uint32_t)&power_mng.amp_hours_measured,
(uint32_t)&energy.e_dc_month_sum[0],
(uint32_t)&android_description,
(uint32_t)&grid_mon[0].f_over.threshold,
(uint32_t)&switch_on_cond.test_time,
(uint32_t)&nsm.pf_hysteresis,
(uint32_t)&battery.stack_software_version[5],
(uint32_t)&grid_mon[0].u_under.time,
(uint32_t)&energy.e_load_total,
(uint32_t)&grid_mon[1].u_over.time,
(uint32_t)&bat_mng_struct.k,
(uint32_t)&energy.e_load_month,
(uint32_t)&power_mng.stop_discharge_current,
(uint32_t)&power_mng.soc_strategy,
(uint32_t)&grid_mon[1].f_under.time,
(uint32_t)&nsm.p_limit,
(uint32_t)&display_struct.contrast,
(uint32_t)&nsm.cos_phi_p[3][0],
(uint32_t)&g_sync.u_ptp_rms[2],
(uint32_t)&logger.month_eac_log_ts,
(uint32_t)&p_buf_available,
(uint32_t)&power_mng.calib_charge_power,
(uint32_t)&io_board.alarm_home_value,
(uint32_t)&buf_v_control.power_reduction_max_solar_grid,
(uint32_t)&g_sync.p_ac_sc[1],
(uint32_t)&bat_mng_struct.k_reserve,
(uint32_t)&nsm.f_entry,
(uint32_t)&logger.minutes_temp_log_ts,
(uint32_t)&db.temp1,
(uint32_t)&dc_conv.last_rescan,
(uint32_t)&battery.cells[0],
(uint32_t)&wifi.connected_ap_ssid,
(uint32_t)&io_board.check_s0_result,
(uint32_t)&rb485.f_grid[1],
(uint32_t)&bat_mng_struct.count,
(uint32_t)&power_mng.amp_hours,
(uint32_t)&energy.e_dc_day[1],
(uint32_t)&battery.module_sn[0],
(uint32_t)&energy.e_grid_load_day_sum,
(uint32_t)&energy.e_ac_month_sum,
(uint32_t)&energy.e_dc_total[0],
(uint32_t)&nsm.rpm_lock_in_power,
(uint32_t)&logger.day_ea_log_ts,
(uint32_t)&grid_mon[1].f_over.time,
(uint32_t)&energy.e_grid_feed_day_sum,
(uint32_t)&buf_v_control.power_reduction,
(uint32_t)&dc_conv.dc_conv_struct[1].enabled };
#define INSERT_VAR_ADDRESSES_HERE

/**
 * Returns index of the variable with given ID.
 * Returns -1 if the ID was not found.
 */
__attribute__ ((section (".ram_code")))
int16_t get_variable_index(uint32_t id) {
	int16_t idx_low = 0;
	int16_t idx_high = COM_FLASH_PARAMETERS - 1;
	while(idx_high - idx_low > 1) {
		int16_t idx = (idx_low + idx_high) >> 1;
		if(id == var_ids[idx])
			return idx;
		else if(id < var_ids[idx])
			idx_high = idx;
		else
			idx_low = idx;
	}
	if(id == var_ids[idx_low]) return idx_low;
	if(id == var_ids[idx_high]) return idx_high;
	return -1;
}

/**
 * Analyzing the input:
 * Write Data ID and Write Data
 * Return:
 * 0 : no errors
 * 1 : size of the input data is not equal to the size of the destination variable
 * 2 : unknown Write Data ID
 * 3 : write access denied
 */
uint16_t i2c_com_parse(uint8_t log_change) {
	int idx = get_variable_index(i2c_com.wr_id);
	int log_idx = get_data_log_variable_index(i2c_com.wr_id);
	if(log_idx >= 0) log_change = 0; // data log request
	else if(i2c_com.wr_id == 0x65A44A98) log_change = 0; // flash_mem array
	else if(i2c_com.wr_id == 0x91617C58) { // g_sync.p_ac_grid_sum_lp
		// Data from Master
		log_change = 0;
		net.master_timeout = 0;
		// If we are not the Slave - ignore this Data
		if(wifi.sockb_protocol != SOCKB_PROT_CLIENT) return 0; // this is not an error
	}
	else if(i2c_com.wr_id == 0x3AA565FC) { // net.package
		// Data from Slave
		log_change = 0;
		net.slave_timeout = 0;
		// Ignore the package if we are neither Master nor Slave
		if(wifi.sockb_protocol == SOCKB_PROT_UNUSED) return 0; // this is not an error
	}
	if(idx >= 0) {
		if(var_size[idx] < i2c_com.wr_data_size) return 1; // wrong size
		switch(var_size[idx]) {
		case 1:
			if(*((uint8_t*)var_addr[idx]) != *((uint8_t*)&i2c_com.wr_data)) {
				if(log_change)
					add_deferred_fault(ERR_PRM_CHANGE, *((uint8_t*)var_addr[idx]), *((uint8_t*)&i2c_com.wr_data), i2c_com.wr_id, 0);
				*((uint8_t*)var_addr[idx]) = *((uint8_t*)&i2c_com.wr_data);
			}
			break;
		case 2:
			if(*((uint16_t*)var_addr[idx]) != *((uint16_t*)&i2c_com.wr_data)) {
				if(log_change)
					add_deferred_fault(ERR_PRM_CHANGE, *((uint16_t*)var_addr[idx]), *((uint16_t*)&i2c_com.wr_data), i2c_com.wr_id, 0);
				*((uint16_t*)var_addr[idx]) = *((uint16_t*)&i2c_com.wr_data);
			}
			break;
		case 4:
			if(*((uint32_t*)var_addr[idx]) != i2c_com.wr_data) {
				if(log_change)
					add_deferred_fault(ERR_PRM_CHANGE, *((uint32_t*)var_addr[idx]), i2c_com.wr_data, i2c_com.wr_id, 0);
				*((uint32_t*)var_addr[idx]) = i2c_com.wr_data;
			}
			break;
		default: {
			// arrays
			uint8_t changed_flag = 0;
			for(int i = 0; i < var_size[idx]; i++) {
				if(i < i2c_com.wr_data_size) {
					if(*((char*)(var_addr[idx] + i)) != i2c_com.wr_data_array[i]) {
						changed_flag = 1;
						*((char*)(var_addr[idx] + i)) = i2c_com.wr_data_array[i];
					}
				}
				else {
					if(*((char*)(var_addr[idx] + i)) != 0) {
						changed_flag = 1;
						*((char*)(var_addr[idx] + i)) = 0;
					}
					break;
				}
			}
			if(i2c_com.wr_id == 0x65A44A98) can_bus.bms_update_flag = 1; // flash_mem array received?
			else if(log_change && changed_flag)
				add_deferred_fault(ERR_PRM_CHANGE, *((uint32_t*)var_addr[idx]), i2c_com.wr_data, i2c_com.wr_id, 0);
		}
		}
		// Monitor communication with COM
		if(i2c_com.wr_id == 0x65EED11B) { // battery.voltage?
			power_mng.com_last_update_time = 0;
			power_mng.com_is_connected = 1;
		}
		else if(i2c_com.wr_id == 0xEBC62737) { // android_description?
			android_description_is_changed = 1;
		}
		if(i2c_com.wr_id == 0x3AA565FC) { // net.package
			unpack_data();
		}
		else data_logger_read(i2c_com.wr_id);
		return 0;
	}
	return 2; // unknown ID
}

/**
 * Returns the raw value of the variable from the given address with given size.
 */
__attribute__ ((section (".ram_code")))
uint32_t read_raw_variable_value(uint8_t* addr, uint16_t size) {
	uint32_t result = 0;
	if(size <= 4) {
		// reversed order of bytes
		addr += size;
		for(int i = 0; i < size; i++) {
			addr--;
			result <<= 8;
			result |= *addr;
		}
	}
	return result;
}

/**
 * Prepare output data for given Read Data ID
 * Return:
 * 0 : no errors
 * 1 : unknown Read Data ID
 * 2 : read canceled
 */
uint16_t i2c_com_prepare_data(void) {
	int idx = get_variable_index(i2c_com.rd_id);
	if(idx >= 0) {
		//if((var_access[idx] & 0b10) == 0) return 2; // read access denied
		i2c_com.rd_data_var_size = var_size[idx];
		// logger.error_log_time_stamp?
		//if(i2c_com.rd_id == 0x6F3876BC) {
		if(var_size[idx] <= 4) {
			// base data types
			i2c_com.rd_data_size = var_size[idx];
			i2c_com.rd_data = (uint8_t*)var_addr[idx];
		}
		else {
			// strings (char array)
			uint8_t* char_p = (uint8_t*)(var_addr[idx]);
			i2c_com.rd_data_size = 0;
			for(int i = 0; i < var_size[idx]; i++) {
				i2c_com.rd_data_size++;
				i2c_com.rd_data_array[i] = *char_p;
				char_p++;
			}
		}

		return 0;
	}
	return 1; // unknown ID
}

/**
 * Returns the raw value of the variable with the given index.
 */
__attribute__ ((section (".ram_code")))
uint32_t get_variable_value(uint16_t idx) {
	i2c_com.rd_id = var_ids[idx];
	i2c_com_prepare_data();
	return read_raw_variable_value(i2c_com.rd_data, i2c_com.rd_data_size);
}

/**
 * Set value of the variable with given id.
 */
void set_variable_value(uint32_t id) {
	int idx = get_variable_index(id);
	if(idx >= 0) {
		i2c_com.wr_id = id;
		i2c_com.wr_data_size = var_size[idx];
		switch(i2c_com.wr_data_size) {
			case 1:
				i2c_com.wr_data = i2c_com.wr_data_array[0];
				break;
			case 2:
				i2c_com.wr_data = ((uint32_t)i2c_com.wr_data_array[1] << 8) | (uint32_t)i2c_com.wr_data_array[0];
				break;
			case 4:
				i2c_com.wr_data = ((uint32_t)i2c_com.wr_data_array[3] << 24) | ((uint32_t)i2c_com.wr_data_array[2] << 16) | ((uint32_t)i2c_com.wr_data_array[1] << 8) | (uint32_t)i2c_com.wr_data_array[0];
				break;
		}
		i2c_com_parse(0);
	}
}

// }@
