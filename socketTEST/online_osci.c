/**
 * @addtogroup online_osci
 * @{
 * online_osci.c
 * @brief COM and OnlineOsci communication protocols support.
 *
 *  Created on: 29.07.2013
 *      Author: dpo
 */

#include "online_osci.h"
#include "I2C_COM_parser.h"

/** Choice between single and multi-block DMA usage */
//#define OSCI_SINGLE_BLOCK_DMA

OsciStructType osci_struct = {
	.state = OSCI_RX_STATE_IDLE,
	.outbuf_write_pos = 0,
	.outbuf_read_pos = 0,
	.osci_ct = 0xAA,
	.ch_type = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, // OSCI_CH_NUMBER 0's
	.protocol = 0,
	.escape = 0,
	.ch_reconf = 0,
	.ch_num_prev = -1,
	.error = 0,
	.error_ct = 0,
	.inbuf_write_pos = 0,
	.inbuf_read_pos = 0,
	.last_byte_time_elapsed = 0,
	.bootloader_detected = 0
};

/** Linked List with up to 12 blocks, one header and one termination block (row 1 type) */
LLI_STRUCT OsciLL[OSCI_CH_NUMBER + 2];


/**
 * Very special variable.
 * This variable is regularly polled. If it is not 0 - it will be interpreted as later described and set back to 0.
 * Value:	Description:
 * 0		No meaning
 * 1		Switching to Online Osci protocol
 * 2		Switching to COM protocol
 * 3		Switching to RAM bootloader
 */
uint8_t com_service = 0;
uint8_t com_service_accepted = 0;

void reconfigure_osci_list(void);

/**
 * Singleton to Reset the GPDMA0 Unit
 */
void GPDMA0_Reset(void) {
	static uint16_t GPDMA0_initialized = 0;
	if(GPDMA0_initialized) return;
	// Assert reset is not needed (reset state is default after CPU reset)
	// SCU_RESET->PRSET2 |= SCU_RESET_PRSET2_DMA0RS_Msk;
	// De-assert reset
	SCU_RESET->PRCLR2 |= SCU_RESET_PRCLR2_DMA0RS_Msk;
	GPDMA0->DMACFGREG |= GPDMA0_DMACFGREG_DMA_EN_Msk;
	GPDMA0_initialized++;
}

/**
 * GPDMA0 configuration
 */
void init_dma_osci(void) {
	//SCU_RESET->PRCLR2 |= SCU_RESET_PRCLR2_DMA0RS_Msk;
	//GPDMA0->DMACFGREG |= GPDMA0_DMACFGREG_DMA_EN_Msk;
	GPDMA0_Reset();
	reconfigure_osci_list();
	WR_REG(DLR->SRSEL0, DLR_SRSEL0_RS0_Msk, DLR_SRSEL0_RS0_Pos, 10); 								// DMA Request Source for Line 0 is USIC0.SR0
	DLR->LNEN |= DLR_LNEN_LN0_Msk; 																	// Enables the line 0 and resets a pending request
	CLR_BIT(GPDMA0_CH0->CFGL, GPDMA0_CH_CFGL_HS_SEL_DST_Pos); 										// Destination Hardware Handshaking Select
	WR_REG(GPDMA0_CH0->CFGH, GPDMA0_CH_CFGH_DEST_PER_Msk, GPDMA0_CH_CFGH_DEST_PER_Pos, 0);
}

/**
 * DMA configuration for USIC0 Channel 0
 */
void init_dma_usic_osci(void) {
	USIC0_CH0->FMR |= USIC_CH_FMR_SIO0_Msk; 														// The service request output SR0 is activated
	WR_REG(USIC0_CH0->TBCTR, USIC_CH_TBCTR_LIMIT_Msk, USIC_CH_TBCTR_LIMIT_Pos, 1); 					// Limit For Interrupt Generation
  	WR_REG(USIC0_CH0->TBCTR, USIC_CH_TBCTR_STBTM_Msk, USIC_CH_TBCTR_STBTM_Pos, 0); 					// Standard Transmit Buffer Trigger Mode
  	WR_REG(USIC0_CH0->TBCTR, USIC_CH_TBCTR_STBTEN_Msk, USIC_CH_TBCTR_STBTEN_Pos, 1); 				// Standard Transmit Buffer Trigger Enable
  	WR_REG(USIC0_CH0->TBCTR, USIC_CH_TBCTR_LOF_Msk, USIC_CH_TBCTR_LOF_Pos, 0);						// A standard transmit buffer event occurs when the filling level
  																									// equals the limit value and gets lower due to transmission
  																									// of a data word.
  	WR_REG(USIC0_CH0->TBCTR, USIC_CH_TBCTR_STBIEN_Msk, USIC_CH_TBCTR_STBIEN_Pos, 1); 				// Standard Transmit Buffer Interrupt Enable
}

/**
 * Singleton to Reset the USIC0 Unit
 */
void USIC0_Reset(void) {
	static uint16_t USIC0_initialized = 0;
	if(USIC0_initialized) return;
	// Assert reset
	SCU_RESET->PRSET0 |= SCU_RESET_PRSET0_USIC0RS_Msk;
	// De-assert reset
	SCU_RESET->PRCLR0 |= SCU_RESET_PRCLR0_USIC0RS_Msk;
	USIC0_initialized++;
}

/**
 * UART interface USIC0 Channel 0 initialization
 * Baud Rate selection:
 * baud = 0: 460800
 * baud = 1: 921600
 * baud = 2: 1500000
 * Baud Rate parameters calculated by XMC4500_UART_Calc tool.
 * Standard time_quanta for ASC is 15.
 */
void init_usic(int32_t baud) {
	// 460800
	uint32_t sample_point = 8; 	// USIC_CH_PCR_ASCMode_SP_Pos
	uint32_t step = 755; 		// USIC_CH_FDR_STEP_Pos
	uint32_t fd_mode = 2; 		// USIC_CH_FDR_DM_Pos
	uint32_t time_quanta = 15;	// USIC_CH_BRG_DCTQ_Pos
	uint32_t pdiv = 2;			// USIC_CH_BRG_PDIV_Pos
	uint32_t pctq_value = 3;	// USIC_CH_BRG_PCTQ_Pos
	if(baud == 57600) {
		// 57600
		sample_point = 6; 	// USIC_CH_PCR_ASCMode_SP_Pos
		step = 985; 		// USIC_CH_FDR_STEP_Pos
		fd_mode = 2; 		// USIC_CH_FDR_DM_Pos
		time_quanta = 11;	// USIC_CH_BRG_DCTQ_Pos
		pdiv = 166;			// USIC_CH_BRG_PDIV_Pos
		pctq_value = 0;		// USIC_CH_BRG_PCTQ_Pos
	}
	else if(baud == 115200) {
		// 115200
		sample_point = 3; 	// USIC_CH_PCR_ASCMode_SP_Pos
		step = 985; 		// USIC_CH_FDR_STEP_Pos
		fd_mode = 2; 		// USIC_CH_FDR_DM_Pos
		time_quanta = 5;	// USIC_CH_BRG_DCTQ_Pos
		pdiv = 166;			// USIC_CH_BRG_PDIV_Pos
		pctq_value = 0;		// USIC_CH_BRG_PCTQ_Pos
	}
	else if(baud == 230400) {
		// 230400
		sample_point = 6; 	// USIC_CH_PCR_ASCMode_SP_Pos
		step = 869; 		// USIC_CH_FDR_STEP_Pos
		fd_mode = 2; 		// USIC_CH_FDR_DM_Pos
		time_quanta = 12;	// USIC_CH_BRG_DCTQ_Pos
		pdiv = 16;			// USIC_CH_BRG_PDIV_Pos
		pctq_value = 1;		// USIC_CH_BRG_PCTQ_Pos
	}
	else if(baud == 921600) {
		// 921600
		sample_point = 8; 	// USIC_CH_PCR_ASCMode_SP_Pos
		step = 755; 		// USIC_CH_FDR_STEP_Pos
		fd_mode = 2; 		// USIC_CH_FDR_DM_Pos
		time_quanta = 15;	// USIC_CH_BRG_DCTQ_Pos
		pdiv = 1;			// USIC_CH_BRG_PDIV_Pos
		pctq_value = 2;		// USIC_CH_BRG_PCTQ_Pos
	}
	else if(baud == 1500000) {
		// 1500000
		sample_point = 8; 	// USIC_CH_PCR_ASCMode_SP_Pos
		step = 1019; 		// USIC_CH_FDR_STEP_Pos
		fd_mode = 1; 		// USIC_CH_FDR_DM_Pos
		time_quanta = 15;	// USIC_CH_BRG_DCTQ_Pos
		pdiv = 0;			// USIC_CH_BRG_PDIV_Pos
		pctq_value = 0;		// USIC_CH_BRG_PCTQ_Pos
	}
	else if(baud == 3000000) {
		// 3000000
		sample_point = 7; 	// USIC_CH_PCR_ASCMode_SP_Pos
		step = 384; 		// USIC_CH_FDR_STEP_Pos
		fd_mode = 2; 		// USIC_CH_FDR_DM_Pos
		time_quanta = 14;	// USIC_CH_BRG_DCTQ_Pos
		pdiv = 0;			// USIC_CH_BRG_PDIV_Pos
		pctq_value = 0;		// USIC_CH_BRG_PCTQ_Pos
	}
	else if(baud == 6000000) {
		// 6000000
		sample_point = 7; 	// USIC_CH_PCR_ASCMode_SP_Pos
		step = 768; 		// USIC_CH_FDR_STEP_Pos
		fd_mode = 2; 		// USIC_CH_FDR_DM_Pos
		time_quanta = 14;	// USIC_CH_BRG_DCTQ_Pos
		pdiv = 0;			// USIC_CH_BRG_PDIV_Pos
		pctq_value = 0;		// USIC_CH_BRG_PCTQ_Pos
	}
	else if(baud == 12000000) {
		// 12000000
		sample_point = 5; 	// USIC_CH_PCR_ASCMode_SP_Pos
		step = 1023; 		// USIC_CH_FDR_STEP_Pos
		fd_mode = 1; 		// USIC_CH_FDR_DM_Pos
		time_quanta = 9;	// USIC_CH_BRG_DCTQ_Pos
		pdiv = 0;			// USIC_CH_BRG_PDIV_Pos
		pctq_value = 0;		// USIC_CH_BRG_PCTQ_Pos
	}
	USIC0_Reset();
	USIC0_CH0->CCR = 0; // Disable module (Idle)
	do {
		USIC0_CH0->KSCFG = USIC_CH_KSCFG_MODEN_Msk |
				// The module is switched on and can operate.
				//	After writing 1 to MODEN, it is recommended
				//	to read register KSCFG to avoid pipeline
				//	effects in the control block before accessing
				//	other USIC registers.
				USIC_CH_KSCFG_BPMODEN_Msk;
				// Bit Protection for MODEN
	} while(USIC0_CH0->KSCFG == 0); // read register KSCFG
	USIC0_CH0->PCR_ASCMode = (1UL << USIC_CH_PCR_ASCMode_SMD_Pos & USIC_CH_PCR_ASCMode_SMD_Msk) |
					// Sample Mode
					// This bit field defines the sample mode of the ASC receiver.
					// The selected data input signal can be sampled only once
					// per bit time or three times (in consecutive time quanta).
					// When sampling three times, the bit value shifted in the
					// receiver shift register is given by a majority decision
					// among the three sampled values.
					// 0B Only one sample is taken per bit time. The current
					// input value is sampled.
					// 1B Three samples are taken per bit time and a majority
					// decision is made.
					(0UL << USIC_CH_PCR_ASCMode_STPB_Pos & USIC_CH_PCR_ASCMode_STPB_Msk) |
					// Stop Bits
					// This bit defines the number of stop bits in an ASC frame.
					// 0B The number of stop bits is 1.
					// 1B The number of stop bits is 2.
					(sample_point << USIC_CH_PCR_ASCMode_SP_Pos & USIC_CH_PCR_ASCMode_SP_Msk);
					// Sample Point
					// This bit field defines the sample point of the bit value. The
					// sample point must not be located outside the programmed
					// bit timing (PCR.SP <= BRG.DCTQ).
	USIC0_CH0->DX0CR = (1UL << USIC_CH_DX0CR_DSEL_Pos & USIC_CH_DX0CR_DSEL_Msk) |
					// The data input DX0B is selected.
					(0UL << USIC_CH_DX0CR_DPOL_Pos & USIC_CH_DX0CR_DPOL_Msk);
					// Data Polarity for DXn
					//	This bit defines the signal polarity of the input signal.
					//	0B The input signal is not inverted.
					//	1B The input signal is inverted.

	USIC0_CH0->FDR = (step << USIC_CH_FDR_STEP_Pos & USIC_CH_FDR_STEP_Msk) |
					// 869 / 1024
					// In normal divider mode STEP contains the reload
					// value for RESULT after RESULT has reached 3FFH.
					//	In fractional divider mode STEP defines the value
					//	added to RESULT with each input clock cycle.
					(fd_mode << USIC_CH_FDR_DM_Pos & USIC_CH_FDR_DM_Msk);
					// Divider Mode
					//	This bit fields defines the functionality of the
					//	fractional divider block.
					//	00B The divider is switched off, fFD = 0.
					//	01B Normal divider mode selected.
					//	10B Fractional divider mode selected.
					//	11B The divider is switched off, fFD = 0.

	USIC0_CH0->BRG = (0UL << USIC_CH_BRG_CLKSEL_Pos & USIC_CH_BRG_CLKSEL_Msk) |
					// Clock Selection
					//	This bit field defines the input frequency fPIN
					//	00B The fractional divider frequency fFD is selected.
					//	01B Reserved, no action
					//	10B The trigger signal DX1T defines fPIN. Signal
					//	MCLK toggles with fPIN.
					//	11B Signal MCLK corresponds to the DX1S signal
					//	and the frequency fPIN is derived from the rising
					//	edges of DX1S.
					(0UL << USIC_CH_BRG_PPPEN_Pos & USIC_CH_BRG_PPPEN_Msk) |
					// Enable 2:1 Divider for fPPP
					//	This bit defines the input frequency fPPP.
					//	0B The 2:1 divider for fPPP is disabled.
					//	fPPP = fPIN
					//	1B The 2:1 divider for fPPP is enabled.
					//	fPPP = fMCLK = fPIN / 2.
					(0UL << USIC_CH_BRG_CTQSEL_Pos & USIC_CH_BRG_CTQSEL_Msk) |
					// Input Selection for CTQ
					//	This bit defines the length of a time quantum for the
					//	protocol pre-processor.
					//	00B fCTQIN = fPDIV
					//	01B fCTQIN = fPPP
					//	10B fCTQIN = fSCLK
					//	11B fCTQIN = fMCLK
					(pctq_value << USIC_CH_BRG_PCTQ_Pos & USIC_CH_BRG_PCTQ_Msk) |
					// Pre-Divider for Time Quanta Counter
					//	This bit field defines length of a time quantum tq for
					//	the time quanta counter in the protocol pre-processor.
					//	tQ = (PCTQ + 1) / fCTQIN
					(time_quanta << USIC_CH_BRG_DCTQ_Pos & USIC_CH_BRG_DCTQ_Msk) |
					// Denominator for Time Quanta Counter
					//	This bit field defines the number of time quanta tq
					//	taken into account by the time quanta counter in the
					//	protocol pre-processor.
					(pdiv << USIC_CH_BRG_PDIV_Pos & USIC_CH_BRG_PDIV_Msk) |
					// Divider Mode: Divider Factor to Generate fPDIV
					//	This bit field defines the ratio between the input
					//	frequency fPPP and the divider frequency fPDIV.
					(0UL << USIC_CH_BRG_SCLKOSEL_Pos & USIC_CH_BRG_SCLKOSEL_Msk) |
					// Shift Clock Output Select
					//	This bit field selects the input source for the
					//	SCLKOUT signal.
					//	0B SCLK from the baud rate generator is selected
					//	as the SCLKOUT input source.
					//	1B The transmit shift clock from DX1 input stage is
					//	selected as the SCLKOUT input source.
					//	Note: The setting SCLKOSEL = 1 is used only when
					//	complete closed loop delay compensation is
					//	required for a slave SSC/IIS. The default
					//	setting of SCLKOSEL = 0 should be always
					//	used for all other cases.
					(0UL << USIC_CH_BRG_SCLKCFG_Pos & USIC_CH_BRG_SCLKCFG_Msk);
					// Shift Clock Output Configuration
					//	This bit field defines the level of the passive phase of
					//	the SCLKOUT signal and enables/disables a delay of
					//	half of a SCLK period.
					//	00B The passive level is 0 and the delay is disabled.
					//	01B The passive level is 1 and the delay is disabled.
					//	10B The passive level is 0 and the delay is enabled.
					//	11B The passive level is 1 and the delay is enabled.

	USIC0_CH0->SCTR = (0UL << USIC_CH_SCTR_SDIR_Pos & USIC_CH_SCTR_SDIR_Msk) |
					// Shift Direction
					//	This bit defines the shift direction of the data words for
					//	transmission and reception.
					//	0B Shift LSB first. The first data bit of a data word
					//	is located at bit position 0.
					//	1B Shift MSB first. The first data bit of a data word
					//	is located at the bit position given by bit field
					//	SCTR.WLE.
					(1UL << USIC_CH_SCTR_PDL_Pos & USIC_CH_SCTR_PDL_Msk) |
					// Passive Data Level
					//	This bit defines the output level at the shift data output
					//	signal when no data is available for transmission. The
					//	PDL level is output with the first relevant transmit shift
					//	clock edge of a data word.
					//	0B The passive data level is 0.
					//	1B The passive data level is 1.
					(0UL << USIC_CH_SCTR_DOCFG_Pos & USIC_CH_SCTR_DOCFG_Msk) |
					// Data Output Configuration
					//	This bit defines the relation between the internal shift
					//	data value and the data output signal DOUTx.
					//	X0B DOUTx = shift data value
					//	X1B DOUTx = inverted shift data value
					(1UL << USIC_CH_SCTR_TRM_Pos & USIC_CH_SCTR_TRM_Msk) |
					// Transmission Mode
					//	This bit field describes how the shift control signal is
					//	interpreted by the DSU. Data transfers are only
					//	possible while the shift control signal is active.
					//	00B The shift control signal is considered as inactive
					//	and data frame transfers are not possible.
					//	01B The shift control signal is considered active if it
					//	is at 1-level. This is the setting to be
					//	programmed to allow data transfers.
					//	10B The shift control signal is considered active if it
					//	is at 0-level. It is recommended to avoid this
					//	setting and to use the inversion in the DX2
					//	stage in case of a low-active signal.
					//	11B The shift control signal is considered active
					//	without referring to the actual signal level. Data
					//	frame transfer is possible after each edge of the
					//	signal.
					(7UL << USIC_CH_SCTR_FLE_Pos & USIC_CH_SCTR_FLE_Msk) |
					// Frame Length (8 bit)
					//	This bit field defines how many bits are transferred
					//	within a data frame. A data frame can consist of
					//	several concatenated data words.
					//	If TCSR.FLEMD = 1, the value can be updated
					//	automatically by the data handler.
					(7UL << USIC_CH_SCTR_WLE_Pos & USIC_CH_SCTR_WLE_Msk);
					// Word Length
					//	This bit field defines the data word length (amount of
					//	bits that are transferred in each data word) for
					//	reception and transmission. The data word is always
					//	right-aligned in the data buffer at the bit positions
					//	[WLE down to 0].
					//	If TCSR.WLEMD = 1, the value can be updated
					//	automatically by the data handler.
					//	0H The data word contains 1 data bit located at bit
					//	position 0.
					//	1H The data word contains 2 data bits located at bit
					//	positions [1:0].
					//	...
					//	EH The data word contains 15 data bits located at
					//	bit positions [14:0].
					//	FH The data word contains 16 data bits located at
					//	bit positions [15:0].
	USIC0_CH0->TCSR = (1UL << USIC_CH_TCSR_TDSSM_Pos & USIC_CH_TCSR_TDSSM_Msk) |
					// TBUF Data Single Shot Mode
					//	This bit defines if the data word TBUF data is
					//	considered as permanently valid or if the data should
					//	only be transferred once.
					//	0B The data word in TBUF is not considered as
					//	invalid after it has been loaded into the transmit
					//	shift register. The loading of the TBUF data into
					//	the shift register does not clear TDV.
					//	1B The data word in TBUF is considered as invalid
					//	after it has been loaded into the shift register. In
					//	ASC and IIC mode, TDV is cleared with the TBI
					//	event, whereas in SSC and IIS mode, it is
					//	cleared with the RSI event.
					//	TDSSM = 1 has to be programmed if an
					//	optional data buffer is used.
					(1UL << USIC_CH_TCSR_TDEN_Pos & USIC_CH_TCSR_TDEN_Msk);
					// TBUF Data Enable
					//	This bit field controls the gating of the transmission
					//	start of the data word in the transmit buffer TBUF.
					//	00B A transmission start of the data word in TBUF is
					//	disabled. If a transmission is started, the
					//	passive data level is sent out.
					//	01B A transmission of the data word in TBUF can be
					//	started if TDV = 1.
					//	10B A transmission of the data word in TBUF can be
					//	started if TDV = 1 while DX2S = 0.
					//	11B A transmission of the data word in TBUF can be
					//	started if TDV = 1 while DX2S = 1.
	/**
	 * FIFO Configuration (USIC0_CH0)
	 * (Note: see also the USIC0_CH1 FIFO configuration)
	 */
	USIC0_CH0->TBCTR = (0UL << USIC_CH_TBCTR_DPTR_Pos & USIC_CH_TBCTR_DPTR_Msk) |
			// Data Pointer
			//	This bit field defines the start value for the transmit
			//	buffer pointers when assigning the FIFO entries to
			//	the transmit FIFO buffer. A read always delivers 0.
			//	When writing DPTR while SIZE = 0, both transmitter
			//	pointers TDIPTR and RTDOPTR in register
			//	TRBPTR are updated with the written value and the
			//	buffer is considered as empty. A write access to
			//	DPTR while SIZE > 0 is ignored and does not modify
			//	the pointers.
			(4UL << USIC_CH_TBCTR_SIZE_Pos & USIC_CH_TBCTR_SIZE_Msk);
			// Buffer Size
			//	This bit field defines the number of FIFO entries
			//	assigned to the transmit FIFO buffer.
			//	000B The FIFO mechanism is disabled. The buffer
			//	does not accept any request for data.
			//	001B The FIFO buffer contains 2 entries.
			//	010B The FIFO buffer contains 4 entries.
			//	011B The FIFO buffer contains 8 entries.
			//	100B The FIFO buffer contains 16 entries.
			//	101B The FIFO buffer contains 32 entries.
			//	110B The FIFO buffer contains 64 entries.
			//	111B Reserved

	USIC0_CH0->RBCTR = (16UL << USIC_CH_RBCTR_DPTR_Pos & USIC_CH_RBCTR_DPTR_Msk) |
			// Data Pointer
			//	This bit field defines the start value for the receive
			//	buffer pointers when assigning the FIFO entries to
			//	the receive FIFO buffer. A read always delivers 0.
			//	When writing DPTR while SIZE = 0, both receiver
			//	pointers RDIPTR and RDOPTR in register TRBPTR
			//	are updated with the written value and the buffer is
			//	considered as empty. A write access to DPTR while
			//	SIZE > 0 is ignored and does not modify the
			//	pointers.
			(5UL << USIC_CH_RBCTR_SIZE_Pos & USIC_CH_RBCTR_SIZE_Msk);
			//	Buffer Size
			//	This bit field defines the number of FIFO entries
			//	assigned to the receive FIFO buffer.
			//	000B The FIFO mechanism is disabled. The buffer
			//	does not accept any request for data.
			//	001B The FIFO buffer contains 2 entries.
			//	010B The FIFO buffer contains 4 entries.
			//	011B The FIFO buffer contains 8 entries.
			//	100B The FIFO buffer contains 16 entries.
			//	101B The FIFO buffer contains 32 entries.
			//	110B The FIFO buffer contains 64 entries.
			//	111B Reserved

	USIC0_CH0->CCR = (2UL << USIC_CH_CCR_MODE_Pos & USIC_CH_CCR_MODE_Msk);
			// 2H The ASC (SCI, UART) protocol is selected.
}

/**
 * Initialization of Online Oscilloscope
 */
void online_osci_init(uint32_t baud) {
	// clean output buffer
	osci_struct.outbuf_read_pos = osci_struct.outbuf_write_pos;
	if(wifi.detect_baud == baud) return;
	wifi.detect_baud = baud;
	// Configure input pin (P1.4)
	WR_REG(PORT1->IOCR4, PORT1_IOCR4_PC4_Msk, PORT1_IOCR4_PC4_Pos, 0);
	// Configure output pin (P1.5) (ALT2)
	WR_REG(PORT1->IOCR4, PORT1_IOCR4_PC5_Msk, PORT1_IOCR4_PC5_Pos, 0b10010);
	init_usic(baud);
	/**
	 * Configure DMA
	 */
	init_dma_osci();
	init_dma_usic_osci();
	/**
	 * Test
	 * Interrupt USIC0.SR0
	 */
	//NVIC_EnableIRQ((IRQn_Type)84);
	/**
	 * Configure CRC Engine (FCE) channel 2 (CCITT CRC16)
	 */
	FCE->CLC = 0x0; // Enable FCE
	FCE_KE2->CFG = 0; // Default settings
}

uint8_t crc_state = 0;
uint16_t crc_fiter = 0;
void init_crc(void) {
	crc_state = 0;
	FCE_KE2->CRC = 0xFFFF; // CRC Seed
}

void fit_crc(uint8_t next_byte) {
	crc_state ^= 1;
	if(crc_state) {
		crc_fiter = next_byte << 8;
	}
	else {
		crc_fiter |= next_byte;
		FCE_KE2->IR = crc_fiter;
	}
}

uint16_t get_crc(void) {
	if(crc_state) {
		FCE_KE2->IR = crc_fiter;
		crc_state = 0;
		__NOP();
	}
	return FCE_KE2->RES;
}

int uart_stream_limiter = 0;
/**
 * Write output buffer into output FIFO
 */
__attribute__ ((section (".ram_code")))
void uart_com_flush_output_buffer() {
	if(osci_struct.bootloader_detected) return;
	if(osci_struct.protocol == OSCI_PROT_COM) {
		if(++uart_stream_limiter < 32) return;
	}
	uart_stream_limiter = 0;
	while(osci_struct.outbuf_read_pos != osci_struct.outbuf_write_pos) {
		if(USIC0_CH0->TRBSR & USIC_CH_TRBSR_TFULL_Msk) break;
		USIC0_CH0->IN[0] = osci_struct.outbuf[osci_struct.outbuf_read_pos];
		osci_struct.outbuf_read_pos++;
		if(osci_struct.outbuf_read_pos >= OSCI_OUTBUF_SIZE) osci_struct.outbuf_read_pos = 0;
	}
}

/**
 * Write one byte into output buffer
 */
void uart_com_write_byte(uint8_t b) {
	osci_struct.outbuf[osci_struct.outbuf_write_pos] = b;
	osci_struct.outbuf_write_pos++;
	if(osci_struct.outbuf_write_pos >= OSCI_OUTBUF_SIZE) osci_struct.outbuf_write_pos = 0;
}

/**
 * Free space in output buffer [byte]
 */
uint32_t get_free_outbuffer_space() {
	if(osci_struct.outbuf_read_pos <= osci_struct.outbuf_write_pos)
		return (OSCI_OUTBUF_SIZE - osci_struct.outbuf_write_pos + osci_struct.outbuf_read_pos);
	else
		return (osci_struct.outbuf_read_pos - osci_struct.outbuf_write_pos);
}

/**
 * Write n bytes to FIFO
 * Order: MSB first
 */
void uart_com_write_fifo_bytes(uint32_t val, uint16_t n) {
	uint8_t shift = (n - 1) * 8;
	for(int i = 0; i < n; i++) {
		uint8_t next_byte = (val >> shift) & 0xFF;
		// inject escape symbol '-'(0x2D)
		if(next_byte == 0x2B || next_byte == 0x2D) uart_com_write_byte(0x2D);
		fit_crc(next_byte);
		uart_com_write_byte(next_byte);
		shift -= 8;
	}
}

/**
 * Erases osci errors after error suspend time OSCI_ERROR_SUSPEND_TIME
 * Call period 100 ms
 */
void online_osci_error_suspend_timer() {
	if(osci_struct.error_ct) {
		if(--osci_struct.error_ct == 0) {
			osci_struct.error = 0;
		}
	}
}

uint16_t osci_detect_chr = 0;
/**
 * Call frequency 10kHz
 */
__attribute__ ((section (".ram_code")))
void online_osci_receive_fifo_bytes(void) {
	int i = 16; // Max allowed loops per function call
	// Read data from Rx FIFO, Rx FIFO has new data?
	while(((USIC0_CH0->TRBSR & USIC_CH_TRBSR_REMPTY_Msk) == 0) && (i--)) {
		uint16_t in_chr = (uint16_t)USIC0_CH0->OUTR;
		osci_struct.inbuf[osci_struct.inbuf_write_pos++] = in_chr;
		osci_detect_chr = (osci_detect_chr << 8) | in_chr;
		if(osci_detect_chr == 0x2BAA) osci_struct.osci_detect_ct++;
		if(osci_struct.inbuf_write_pos >= OSCI_INBUF_SIZE) osci_struct.inbuf_write_pos = 0;
	}
}

/**
 * Increments inbuf_read_pos, set it to 0 if necessary
 */
void increment_read_pos() {
	osci_struct.inbuf_read_pos++;
	if(osci_struct.inbuf_read_pos >= OSCI_INBUF_SIZE) osci_struct.inbuf_read_pos = 0;
}

/**
 * Send selected variable to COM port
 * cmd_selector = 0:response, 1:write
 */
void implement_read(int cmd_selector) {
	int res = i2c_com_prepare_data();
	if(res == 0) {
		uart_com_write_byte(0x2B); // Start
		init_crc();
		if(i2c_com.rd_data_size > 251) {
			if(cmd_selector == 0)
				uart_com_write_fifo_bytes(0x06, 1); // Long Response
			else
				uart_com_write_fifo_bytes(0x03, 1); // Long Write
			uart_com_write_fifo_bytes(4 + i2c_com.rd_data_size, 2); // Length
		}
		else {
			if(cmd_selector == 0)
				uart_com_write_fifo_bytes(0x05, 1); // Response
			else
				uart_com_write_fifo_bytes(0x02, 1); // Write
			uart_com_write_fifo_bytes(4 + i2c_com.rd_data_size, 1); // Length
		}
		uart_com_write_fifo_bytes(i2c_com.rd_id, 4); // ID
		if(i2c_com.rd_data_var_size <= 4) {
			// base data types
			// reversed order of bytes
			i2c_com.rd_data += i2c_com.rd_data_size;
			for(int i = 0; i < i2c_com.rd_data_size; i++) {
				i2c_com.rd_data--;
				uart_com_write_fifo_bytes(*i2c_com.rd_data, 1); // Data
			}
		}
		else {
			// arrays
			for(int i = 0; i < i2c_com.rd_data_size; i++) {
				uart_com_write_fifo_bytes(i2c_com.rd_data_array[i], 1); // Data
			}
		}
		osci_struct.com_out_crc16 = get_crc();
		uart_com_write_fifo_bytes(osci_struct.com_out_crc16, 2); // CRC-16
	}
	/*else {
		osci_struct.error |= 1 << OSC_ERR_UNKNOWN_READ_ID;
		osci_struct.error_ct = OSCI_ERROR_SUSPEND_TIME;
	}*/
}

/**
 * Send variable with ID = var_id to COM port
 */
void read_var_and_send(uint32_t var_id, int cmd_selector) {
	i2c_com.rd_id = var_id;
	implement_read(cmd_selector);
}

void com_protocol() {
	uint16_t res;
	// data logger response is ready?
	//if(logger.response_flag != DL_REQ_NONE && osci_struct.state == OSCI_RX_STATE_IDLE) {
	//	osci_struct.inbuf_read_pos = osci_struct.inbuf_write_pos;
	//	osci_struct.escape = 0;
	//	return;
	//}
	if(get_free_outbuffer_space() < DATA_LOGGER_BUFFER) return;
	// "Start Byte" '+'(0x2B)?
	if(osci_struct.inbuf[osci_struct.inbuf_read_pos] == 0x2B) {
		if(osci_struct.escape) {
			osci_struct.escape = 0;
			if(osci_struct.state == OSCI_RX_STATE_IDLE) {
				// Wrong "Start Byte" detected (escaped)
				osci_struct.error |= 1 << OSC_ERR_ESCAPED_START;
				osci_struct.error_ct = OSCI_ERROR_SUSPEND_TIME;
			}
		}
		else if(osci_struct.state != OSCI_RX_STATE_IDLE) {
			// Incorrect "Start Byte" detected (previous data was not completed)
			osci_struct.error |= 1 << OSC_ERR_DATA_INCOMPLETE;
			osci_struct.error_ct = OSCI_ERROR_SUSPEND_TIME;
			osci_struct.state = OSCI_RX_STATE_IDLE;
		}
	}
	// Escape symbol '-'(0x2D) detection
	else if(osci_struct.escape == 0 && osci_struct.inbuf[osci_struct.inbuf_read_pos] == 0x2D) {
		osci_struct.escape = 1;
		increment_read_pos();
		return;
	}
	else osci_struct.escape = 0;
	switch(osci_struct.state) {
		case OSCI_RX_STATE_IDLE: // Waiting for Start byte (0x2B)
			if(osci_struct.inbuf[osci_struct.inbuf_read_pos] == 0x2B) {
				init_crc();
				osci_struct.state = OSCI_RX_STATE_CMD;
			}
			else {
				// Data without start
				osci_struct.error |= 1 << OSC_ERR_DATA_WITHOUT_START;
				osci_struct.error_ct = OSCI_ERROR_SUSPEND_TIME;
			}
			break;
		case OSCI_RX_STATE_CMD: // COMMAND byte
			osci_struct.com_cmd = osci_struct.inbuf[osci_struct.inbuf_read_pos];
			fit_crc(osci_struct.com_cmd);
			if(osci_struct.com_cmd > 0x3) {
				// Unknown command
				// Dangerous! Spontaneous switching to OSCI protocol!
				/*if(osci_struct.com_cmd == 0xAA) {
					// Online Osci protocol?
					osci_struct.protocol = OSCI_PROT_OSCILL;
					return;
				}
				else {
					osci_struct.error |= 1 << OSC_ERR_UNKNOWN_CMD;
					osci_struct.error_ct = OSCI_ERROR_SUSPEND_TIME;
					osci_struct.state = OSCI_RX_STATE_IDLE;
				}*/
				osci_struct.state = OSCI_RX_STATE_IDLE;
			}
			else osci_struct.state = OSCI_RX_STATE_LENGTH;
			osci_struct.com_length = 0;
			osci_struct.inbuf_ct = 0;
			break;
		case OSCI_RX_STATE_LENGTH: // Length
			osci_struct.inbuf_ct++;
			osci_struct.com_length <<= 8;
			osci_struct.com_length |= osci_struct.inbuf[osci_struct.inbuf_read_pos];
			fit_crc(osci_struct.inbuf[osci_struct.inbuf_read_pos]);
			// "long" command?
			if((osci_struct.com_cmd == 0x3 || osci_struct.com_cmd == 0x6) && osci_struct.inbuf_ct < 2) break;
			i2c_com.wr_data_size = osci_struct.com_length - 4;
			if(i2c_com.wr_data_size < 0 || i2c_com.wr_data_size > MAX_COM_DATA_SIZE) {
				osci_struct.state = OSCI_RX_STATE_IDLE;
			}
			else {
				osci_struct.state = OSCI_RX_STATE_ID;
				osci_struct.inbuf_ct = 0;
				i2c_com.rd_id = 0;
			}
			break;
		case OSCI_RX_STATE_ID: // ID
			osci_struct.inbuf_ct++;
			fit_crc(osci_struct.inbuf[osci_struct.inbuf_read_pos]);
			i2c_com.rd_id <<= 8;
			i2c_com.rd_id |= osci_struct.inbuf[osci_struct.inbuf_read_pos];
			if(osci_struct.inbuf_ct == 4) {
				osci_struct.inbuf_ct = 0;
				// "WRITE" commands?
				if(osci_struct.com_cmd == 0x2 || osci_struct.com_cmd == 0x3) {
					i2c_com.wr_id = i2c_com.rd_id;
					i2c_com.wr_data = 0;
					osci_struct.state = OSCI_RX_STATE_DATA;
				}
				// "READ" command
				else {
					osci_struct.last_byte_time_elapsed = 0;
					osci_struct.com_in_crc16 = 0;
					osci_struct.state = OSCI_RX_STATE_CRC;
				}
			}
			break;
		case OSCI_RX_STATE_DATA: // Data
			fit_crc(osci_struct.inbuf[osci_struct.inbuf_read_pos]);
			// receive base type
			i2c_com.wr_data <<= 8;
			i2c_com.wr_data |= osci_struct.inbuf[osci_struct.inbuf_read_pos];
			// receive array
			i2c_com.wr_data_array[osci_struct.inbuf_ct] = osci_struct.inbuf[osci_struct.inbuf_read_pos];
			osci_struct.inbuf_ct++;
			if(osci_struct.inbuf_ct == i2c_com.wr_data_size) {
				osci_struct.inbuf_ct = 0;
				osci_struct.com_in_crc16 = 0;
				osci_struct.state = OSCI_RX_STATE_CRC;
			}
			break;
		case OSCI_RX_STATE_CRC:
			osci_struct.inbuf_ct++;
			osci_struct.com_in_crc16 <<= 8;
			osci_struct.com_in_crc16 |= osci_struct.inbuf[osci_struct.inbuf_read_pos];
			if(osci_struct.inbuf_ct == 2) {
				// Check CRC
				osci_struct.com_calc_crc16 = get_crc();
				if(osci_struct.com_calc_crc16 == osci_struct.com_in_crc16) {
					switch(osci_struct.com_cmd) {
						case 0x01: // Read
							// answer only if the output buffer is empty
							//if(osci_struct.outbuf_read_pos == osci_struct.outbuf_write_pos) {
							implement_read(0);
							//}
							break;
						case 0x02: // Write
						case 0x03: // Long Write
							res = i2c_com_parse(1);
							if(res == 0) {
							}
							else {
								if(res == 1) {
									osci_struct.error |= 1 << OSC_ERR_WRONG_WRITE_SIZE;
									osci_struct.error_ct = OSCI_ERROR_SUSPEND_TIME;
								}
								else {
									osci_struct.error |= 1 << OSC_ERR_UNKNOWN_WRITE_ID;
									osci_struct.error_ct = OSCI_ERROR_SUSPEND_TIME;
								}
							}
							break;
					}
				}
				else {
					osci_struct.error |= 1 << OSC_ERR_WRONG_CRC;
					osci_struct.error_ct = OSCI_ERROR_SUSPEND_TIME;
				}
				osci_struct.state = OSCI_RX_STATE_IDLE;
			}
			break;
	} // switch(osci_struct.state)
	increment_read_pos();
}

void osci_protocol() {
	static uint16_t ch_num = 0, data_type = 0;
	static uint64_t data = 0;
	switch(osci_struct.state) {
		case OSCI_RX_STATE_IDLE:
			if(osci_struct.inbuf[osci_struct.inbuf_read_pos] == 0x2B) osci_struct.state = OSCI_RX_STATE_CMD;
			break;
		case OSCI_RX_STATE_CMD:
			switch(osci_struct.inbuf[osci_struct.inbuf_read_pos]) {
				//case 0x1:
				//case 0x2:
					// COM protocol?
					//osci_struct.protocol = OSCI_PROT_COM;
					//return;
				case 0x3C: // Extended set of commands
					osci_struct.state = OSCI_RX_STATE_CMD_EXT;
					break;
				case 0xAA: // Configuring Channel for Variable Reading
					osci_struct.inbuf_ct = 0;
					osci_struct.state = OSCI_RX_STATE_VAR_READ;
					break;
				case 0xBB: // Configuring Channel for Variable Writing
					osci_struct.inbuf_ct = 0;
					osci_struct.state = OSCI_RX_STATE_VAR_WRITE;
					break;
				default:
					osci_struct.state = OSCI_RX_STATE_IDLE;
			}
			break;
		case OSCI_RX_STATE_CMD_EXT:
			switch(osci_struct.inbuf[osci_struct.inbuf_read_pos]) {
				case 0xD0: // Reset DSP
					EmergencyStop(13);
					NVIC_SystemReset();
					break;
				case 0xD1: // Request Acknowledge
					break;
				case 0xE0: // Internal Boot Mode
					EmergencyStop(14);
					flash_state_queue = 3;
					break;
				case 0xE1: // Switching to COM protocol
					osci_struct.protocol = OSCI_PROT_COM;
					break;
			}
			osci_struct.state = OSCI_RX_STATE_IDLE;
			break;
		case OSCI_RX_STATE_VAR_READ:
			/*
			 * Byte 0: 0x2B <= already received
			 * Byte 1: 0xAA <= already received
			 * Byte 2: Channel (0..11)
			 * Byte 3: Variable Type (0 � Variable not used, 1 � 8 Bit, 2 � 16 Bit, 4 � 32 Bit, 8 � 64 Bit)
			 * Byte 4: Variable 32-Bit Address Byte 0 (LSBF)
			 * Byte 5: Variable 32-Bit Address Byte 1 (LSBF)
			 * Byte 6: Variable 32-Bit Address Byte 2 (LSBF)
			 * Byte 7: Variable 32-Bit Address Byte 3 (LSBF)
			 * Byte 8: 0xFE
			 */
			osci_struct.inbuf_ct++;
			switch(osci_struct.inbuf_ct) {
				case 1:
					ch_num = osci_struct.inbuf[osci_struct.inbuf_read_pos];
					if(ch_num == 0) {
						// Prepare to accumulate the addresses sum
						osci_struct.osci_addr_sum = 0xAA;
						// Erase all other channels if the channel 0 was right now configured
						for(int i = 1; i < OSCI_CH_NUMBER; i++) osci_struct.ch_type[i] = 0;
						osci_struct.ch_num_prev = -1;
						osci_struct.ch_reconf = 1;
					}
					if(ch_num != ++osci_struct.ch_num_prev) {
						// wrong channel number detected
						osci_struct.error |= 1 << OSC_ERR_WRONG_CHANNEL;
						osci_struct.error_ct = OSCI_ERROR_SUSPEND_TIME;
						osci_struct.state = OSCI_RX_STATE_IDLE;
					}
					break;
				case 2:
					osci_struct.ch_type[ch_num] = osci_struct.inbuf[osci_struct.inbuf_read_pos];
					if(osci_struct.ch_type[ch_num] > 2 && osci_struct.ch_type[ch_num] != 4 && osci_struct.ch_type[ch_num] != 8) {
						// wrong channel type detected
						osci_struct.error |= 1 << OSC_ERR_CHANNEL_TYPE;
						osci_struct.error_ct = OSCI_ERROR_SUSPEND_TIME;
						osci_struct.state = OSCI_RX_STATE_IDLE;
					}
					break;
				case 3:
					osci_struct.addr = osci_struct.inbuf[osci_struct.inbuf_read_pos];
					break;
				case 4:
					osci_struct.addr |= ((uint32_t)osci_struct.inbuf[osci_struct.inbuf_read_pos]) << 8;
					break;
				case 5:
					osci_struct.addr |= ((uint32_t)osci_struct.inbuf[osci_struct.inbuf_read_pos]) << 16;
					break;
				case 6:
					osci_struct.addr |= ((uint32_t)osci_struct.inbuf[osci_struct.inbuf_read_pos]) << 24;
					// check the address (see linker file heiphoss.ld)
					// FLASH_1_cached(RX) : ORIGIN = 0x08000000, LENGTH = 0x100000
					// FLASH_1_uncached(RX) : ORIGIN = 0x0C000000, LENGTH = 0x100000
					// PSRAM_1(!RX) : ORIGIN = 0x10000000, LENGTH = 0x10000
					// DSRAM_1_system(!RX) : ORIGIN = 0x20000000, LENGTH = 0x10000
					// DSRAM_2_comm(!RX) : ORIGIN = 0x30000000, LENGTH = 0x8000
					if((osci_struct.addr >= 0x08000000 && osci_struct.addr < (0x08000000 + 0x100000)) ||
							(osci_struct.addr >= 0x0C000000 && osci_struct.addr < (0x0C000000 + 0x100000)) ||
							(osci_struct.addr >= 0x10000000 && osci_struct.addr < (0x10000000 + 0x10000)) ||
							(osci_struct.addr >= 0x20000000 && osci_struct.addr < (0x20000000 + 0x10000)) ||
							(osci_struct.addr >= 0x30000000 && osci_struct.addr < (0x30000000 + 0x8000))) {
						osci_struct.ch_addr[ch_num] = osci_struct.addr; // Address changed atomic
					}
					else if(osci_struct.ch_type[ch_num] != 0) {
						// wrong address detected
						osci_struct.error |= 1 << OSC_ERR_READ_ADDR;
						osci_struct.error_ct = OSCI_ERROR_SUSPEND_TIME;
						osci_struct.ch_type[ch_num] = 0;
						osci_struct.state = OSCI_RX_STATE_IDLE;
					}
					break;
				case 7:
					if(osci_struct.inbuf[osci_struct.inbuf_read_pos] != 0xFE) {
						// The end of frame is incorrect
						osci_struct.error |= 1 << OSC_ERR_FRAME;
						osci_struct.error_ct = OSCI_ERROR_SUSPEND_TIME;
						osci_struct.ch_type[ch_num] = 0;
					}
					else {
						// Accumulate the addresses sum
						if(osci_struct.ch_type[ch_num]) {
							osci_struct.osci_addr_sum += osci_struct.ch_addr[ch_num];
						}
						// reconfiguration only for the last channel
						if(ch_num == OSCI_CH_NUMBER - 1) {
							reconfigure_osci_list();
							osci_struct.ch_num_prev = -1;
						}
					}
					osci_struct.state = OSCI_RX_STATE_IDLE;
					break;
			}
			break;
		case OSCI_RX_STATE_VAR_WRITE:
			/*
			 * Byte 0: 0x2B <= already received
			 * Byte 1: 0xBB <= already received
			 * Byte 2: Variable Type (0 � Variable nicht verwendet, 1 � 8 Bit, 2 � 16 Bit, 4 � 32 Bit, 8 � 64 Bit)
			 * Byte 3: Variable 32-Bit Adresse Byte 0 (LSBF)
			 * Byte 4: Variable 32-Bit Adresse Byte 1 (LSBF)
			 * Byte 5: Variable 32-Bit Adresse Byte 2 (LSBF)
			 * Byte 6: Variable 32-Bit Adresse Byte 3 (LSBF)
			 * Variabelwert (Anzahl von Bytes ist abh�ngig von Variable Type)
			 * Checksum 16 (2 Bytes LSBF) die Summe von alle Bytes (als unsigned char) �ber die ganze Sendung au�er Checksum selbst
			 */
			osci_struct.inbuf_ct++;
			switch(osci_struct.inbuf_ct) {
				case 1:
					data_type = osci_struct.inbuf[osci_struct.inbuf_read_pos];
					if(data_type == 0 || (data_type > 2 && data_type != 4 && data_type != 8)) {
						// wrong data type detected
						osci_struct.state = OSCI_RX_STATE_IDLE;
					}
					osci_struct.check_sum = (uint16_t)0x2B + (uint16_t)0xBB + data_type;
					break;
				case 2:
					osci_struct.addr = osci_struct.inbuf[osci_struct.inbuf_read_pos];
					osci_struct.check_sum += osci_struct.inbuf[osci_struct.inbuf_read_pos];
					break;
				case 3:
					osci_struct.addr |= osci_struct.inbuf[osci_struct.inbuf_read_pos] << 8;
					osci_struct.check_sum += osci_struct.inbuf[osci_struct.inbuf_read_pos];
					break;
				case 4:
					osci_struct.addr |= osci_struct.inbuf[osci_struct.inbuf_read_pos] << 16;
					osci_struct.check_sum += osci_struct.inbuf[osci_struct.inbuf_read_pos];
					break;
				case 5:
					osci_struct.addr |= osci_struct.inbuf[osci_struct.inbuf_read_pos] << 24;
					osci_struct.check_sum += osci_struct.inbuf[osci_struct.inbuf_read_pos];
					data = 0;
					break;
				case 6:
				case 7:
				case 8:
				case 9:
				case 10:
				case 11:
				case 12:
				case 13:
					data += ((uint64_t)osci_struct.inbuf[osci_struct.inbuf_read_pos]) << ((osci_struct.inbuf_ct - 6) * 8);
					osci_struct.check_sum += osci_struct.inbuf[osci_struct.inbuf_read_pos];
					if(osci_struct.inbuf_ct - 5 == data_type) {
						osci_struct.inbuf_ct = 0;
						osci_struct.state = OSCI_RX_STATE_VAR_WRITE_CRC;
					}
					break;
			}
			break;
		case OSCI_RX_STATE_VAR_WRITE_CRC:
			osci_struct.inbuf_ct++;
			switch(osci_struct.inbuf_ct) {
				case 1:
					osci_struct.must_check_sum = osci_struct.inbuf[osci_struct.inbuf_read_pos];
					break;
				case 2:
					osci_struct.must_check_sum += (osci_struct.inbuf[osci_struct.inbuf_read_pos] << 8);
					if(osci_struct.check_sum == osci_struct.must_check_sum) {
						switch(data_type) {
							case 1:
								*((uint8_t *)osci_struct.addr) = (uint8_t)data;
								break;
							case 2:
								*((uint16_t *)osci_struct.addr) = (uint16_t)data;
								break;
							case 4:
								*((uint32_t *)osci_struct.addr) = (uint32_t)data;
								break;
							case 8:
								*((uint64_t *)osci_struct.addr) = data;
								break;
						}
					}
					osci_struct.state = OSCI_RX_STATE_IDLE;
					break;
			}
			break;
	} // switch(osci_struct.state)
	increment_read_pos();
}

const uint8_t magic_number[] = {0x50, 0xF7, 0x05, 0xAB};
uint8_t magic_number_p = 0;
/**
 * UART Rx (USIC0_CH0)
 * Evaluation of arriving data.
 * Called asynchrony from the main loop.
 * Some UART channel characteristics:
 * Frame size = Data size = 8 bits
 */
void online_osci_receive(void) {
	network_send_data();
	//uart_com_flush_output_buffer();
	if(osci_struct.protocol == OSCI_PROT_WIFI) {
		return;
	}
	// Rx FIFO has new data?
	if(osci_struct.inbuf_read_pos != osci_struct.inbuf_write_pos) {
		osci_struct.last_byte_time_elapsed = 0;
		while(osci_struct.inbuf_read_pos != osci_struct.inbuf_write_pos) {
			// Check "Magic Number" of the bootloader
			if(osci_struct.inbuf[osci_struct.inbuf_read_pos] == magic_number[magic_number_p]) {
				if(++magic_number_p == 4) {
					// Bootloader detected
					osci_struct.bootloader_detected = 120; // 2 minutes pause
					magic_number_p = 0;
				}
			}
			else magic_number_p = 0;
			// wifi protocol will be parsed in wifi.c
			if(osci_struct.protocol == OSCI_PROT_COM) {
				com_protocol();
			}
			else if(osci_struct.protocol == OSCI_PROT_OSCILL) {
				osci_protocol();
			}
		}
	}
}

/**
 * Send Data Log data by request
 * Call period 100 ms
 */
int ls_state = 0; // logger send state
int ls_count;
float ls_chunk_pause;
void send_log_data() {
	if(osci_struct.protocol == OSCI_PROT_COM) {
		if(logger.response_flag != DL_REQ_NONE /*&& get_free_outbuffer_space() > logger.buffer_idx + 64*/) {
			switch(ls_state) {
				case 0: // prepare to send data
					// wait until output buffer is flushed
					if(osci_struct.outbuf_read_pos != osci_struct.outbuf_write_pos) return;
					// flush output buffer
					uart_com_write_byte(0x2B); // Start
					init_crc();
					uart_com_write_fifo_bytes(0x06, 1); // Long Response
					uart_com_write_fifo_bytes(4 + logger.buffer_idx, 2); // Length
					uart_com_write_fifo_bytes(logger.requested_id, 4); // ID
					ls_count = 0;
					ls_state++;
					// break; do not need break here
				case 1:
					// arrays
					if(ls_count < logger.buffer_idx) {
						for(int i = 0; i < logger.buffer_idx; i++) {
							uart_com_write_fifo_bytes(logger.buffer[ls_count], 1); // Data
							ls_count++;
							if(ls_count >= logger.buffer_idx) break;
						}
					}
					if(ls_count < logger.buffer_idx) {
						//ls_state = 3; this will increase the pause between chunks
						ls_chunk_pause = 0;
						break;
					}
					else {
						osci_struct.com_out_crc16 = get_crc();
						uart_com_write_fifo_bytes(osci_struct.com_out_crc16, 2); // CRC-16
						ls_state = 2;
					}
					// break; do not need break here
				case 2:
					// end of transmission
					// wait until output buffer is flushed
					if(osci_struct.outbuf_read_pos != osci_struct.outbuf_write_pos) break;
					logger.response_flag = DL_REQ_NONE;
					ls_state = 0;
					break;
				case 3:
					// pause...
					//ls_chunk_pause += 0.1f;
					//if(ls_chunk_pause > 1.0f)
					ls_state = 1;
					break;
			}
		}
	}
	else {
		// for OSCI_PROT_OSCILL protocol accept response
		if(logger.response_flag != DL_REQ_NONE) logger.response_flag = DL_REQ_NONE;
	}
}

/**
 * This function transmits all the necessary data for "online oscilloscope" functionality
 * in to UART-Tx channel. It should be called periodically (normally 40 kHz).
 * Some UART channel characteristics:
 * Frame size = Data size = 8 bits
 * Protocol:
 * Byte 0: 0xAA
 * Byte 1: Counter (0..255)
 * Channel 0 (LSBF)
 * Channel 1 (LSBF)
 * ...
 */
__attribute__ ((section (".ram_code")))
void online_osci_transmit(void) {
	// DMA channel still enabled?
	if(GPDMA0->CHENREG & (1UL << GPDMA0_CHENREG_CH_Pos)) return;
	// DMA support only for "OnlineOsci"
	if(osci_struct.ch_reconf || osci_struct.protocol != OSCI_PROT_OSCILL || osci_struct.bootloader_detected) return;
	osci_struct.osci_ct += 0x100;
	GPDMA0_CH0->LLP = (uint32_t)&(OsciLL[0]);
	GPDMA0_CH0->CTLH = OsciLL[0].CTLH;
	GPDMA0_CH0->CTLL = OsciLL[0].CTLL;
	GPDMA0->CHENREG |= (1UL << GPDMA0_CHENREG_CH_Pos) | (1UL << GPDMA0_CHENREG_WE_CH_Pos); // Enable channel 0
}

/**
 * Reconfigure the GPDMA0 channel 0 Linked List (Multi-Block Transfer).
 * GPDMA Multi-Block configuration allows automated transfer of the selected variables actual values from RAM to UART.
 * Working mode: row 10 (see Table 5-5 in Reference Manual)
 * Termination mode: row 1
 */
void reconfigure_osci_list(void) {
	int block_idx = 0;
	// Header Block
	OsciLL[0].CTLH = (uint32_t)( GPDMA0_CH_CTLH_BLOCK_TS_Msk & 2); // Block Transfer Size;
	OsciLL[0].CTLL = (2UL << GPDMA0_CH_CTLL_DINC_Pos & GPDMA0_CH_CTLL_DINC_Msk) |	// Destination Address Increment: No change
				     (1UL << GPDMA0_CH_CTLL_TT_FC_Pos & GPDMA0_CH_CTLL_TT_FC_Msk) |	// Transfer Type / Flow Control: Memory to Peripheral / GPDMA
					 GPDMA0_CH_CTLL_LLP_DST_EN_Msk | // Linked List Pointer for Destination Enable
					 GPDMA0_CH_CTLL_LLP_SRC_EN_Msk; // Linked List Pointer for Source Enable
	OsciLL[0].DAR = (uint32_t)&(USIC0_CH0->IN[0]); // Destination Address
	OsciLL[0].SAR = (uint32_t)&(osci_struct.osci_ct); // Source Address
	OsciLL[0].LLP = (uint32_t)&(OsciLL[1]);
	// Data Blocks
	for(uint16_t i = 0; i < OSCI_CH_NUMBER; i++) {
		if(osci_struct.ch_type[i]) {
			block_idx++;
			OsciLL[block_idx].CTLH = (uint32_t)( GPDMA0_CH_CTLH_BLOCK_TS_Msk & osci_struct.ch_type[i]); // Block Transfer Size;
			OsciLL[block_idx].CTLL = OsciLL[0].CTLL;
			OsciLL[block_idx].DAR = OsciLL[0].DAR;
			OsciLL[block_idx].SAR = osci_struct.ch_addr[i];
			OsciLL[block_idx].LLP = (uint32_t)&(OsciLL[block_idx + 1]);
		}
	}
	// Terminate the last Block
	OsciLL[block_idx].CTLL &= ~(GPDMA0_CH_CTLL_LLP_DST_EN_Msk | GPDMA0_CH_CTLL_LLP_SRC_EN_Msk);
	OsciLL[block_idx].LLP = 0;
	osci_struct.ch_reconf = 0;
}

// }@
