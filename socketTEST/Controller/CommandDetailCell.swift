//
//  CommandDetailCell.swift
//  socketTEST
//
//  Created by sasha on 4/25/18.
//  Copyright © 2018 sasha. All rights reserved.
//

import UIKit

class CommandDetailCell: UITableViewCell {

    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }


    func updateCell(name: String, value: String) {
        self.nameLabel.text = name
        self.valueLabel.text = value
    }
    
}
