//
//  ViewController.swift
//  socketTEST
//
//  Created by sasha on 4/16/18.
//  Copyright © 2018 sasha. All rights reserved.
//

import UIKit
import SwiftSocket


class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var commands: CommandStack = CommandStack()
    
    
    
    let intervalSendToServer: Double = 3.0
    let intervalListenSocket: Double = 1
    
    
    var sendToServer = Timer()
    var receive = Timer()
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sendToServer = Timer.scheduledTimer(timeInterval: intervalSendToServer, target: self, selector: #selector(ViewController.sendRequestToServer), userInfo: nil, repeats: true)
        receive = Timer.scheduledTimer(timeInterval: intervalListenSocket, target: self, selector: #selector(ViewController.sendListenerToServer), userInfo: nil, repeats: true)
        NotificationCenter.default.addObserver(self, selector: #selector(updateExternal), name: NSNotification.Name(rawValue: "updateExternal"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateInternal), name: NSNotification.Name(rawValue: "updateInternal"), object: nil)
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    @objc func sendListenerToServer() {
        
         DispatchQueue.global(qos:.userInteractive).async {
            //print("Listener")
            self.commands.listener()
            //print("Listener end working")
        }
        
    }
    
    @objc func sendRequestToServer() {
        if commands.stack.count > 0 {
            //print("Stack count = \(commands.stack.count)")
            return
        }
        
        DispatchQueue.global(qos:.userInteractive).async {
            //print("Send request start")
            TableID.sharedInstanse.cleaned()
            for item in TableID.sharedInstanse.responceTable {
                if(!item.refreshed){
                    self.commands.stack.append(item.id)
                }
            }
        }
    }
    
    
    @objc func updateExternal() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    @objc func updateInternal() {
         DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "commandCell") as! CommandDetailCell
        let frame = TableID.sharedInstanse.responceTable[indexPath.row]
        cell.updateCell(name: frame.description, value: frame.getResult())
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TableID.sharedInstanse.responceTable.count
    }
}

