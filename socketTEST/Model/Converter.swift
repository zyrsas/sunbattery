//
//  Formatter.swift
//  socketTEST
//
//  Created by sasha on 4/18/18.
//  Copyright © 2018 sasha. All rights reserved.
//

import Foundation


class Converter {
    
    static public func parseFloat32(mas: [UInt8], offset: Int) -> Float {
        var f:Float = 0.0
        let massw: Array<UInt8> = [mas[offset + 3], mas[offset + 2], mas[offset + 1], mas[offset]]
        memcpy(&f, massw, 4)
        return f
        
    }
    
    static public func parseInt(mas: [UInt8], offset: Int) -> Int
    {
        var int:Int = 0
        let massw: Array<UInt8> =  [mas[offset + 1], mas[offset]]
        memcpy(&int, massw, 2)
        return int
    }
    
    static public func parseUnsignedInt32(mas: [UInt8], offset: Int) -> UInt
    {
        var uint:UInt = 0
        let massw: Array<UInt8> =  [mas[offset + 3], mas[offset + 2], mas[offset + 1], mas[offset]]
        memcpy(&uint, massw, 4)
        return uint
    }

    static public func parseUnsignedChar(mas: [UInt8], offset: Int) -> String
    {
        let res = String(mas[offset])
        return res
    }
}
