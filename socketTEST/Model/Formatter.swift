//
//  Formatter.swift
//
//
//  Created by sasha on 4/22/18.
//

import Foundation

class Formatter {
    
    private let startByte: UInt8 = 0x2B
    private let readByte: UInt8 = 0x01
    private let lengthByte: UInt8 = 0x04
    private let deviceID: [UInt8]?
    private let stopByte: UInt8 = 0x2D
    
    init(deviceID: [UInt8]) {
        self.deviceID = deviceID
    }
    
    func generateReadComand() -> [UInt8] {
        var CRC: [UInt8]  = []
        CRC.append(readByte)
        CRC.append(lengthByte)
        CRC += deviceID!
        let CRC_calc: [UInt8] = CRC16.instance.getCRCResult(by: CRC)
       
        var command: [UInt8] = []
        command.append(startByte)
        command += CRC
        command += CRC_calc
        var endcount = command.count
        var index = 1
        while(index < endcount){
           switch command[index]{
                case 0x2D:
                    command.insert(0x2D, at: index)
                    index += 1
                    endcount = command.count
                case 0x2B:
                    command.insert(0x2D, at: index)
                    index += 1
                    endcount = command.count
                default:
                    break
                }
            index += 1
        }
        command.append(stopByte)
        return command
    }
}
