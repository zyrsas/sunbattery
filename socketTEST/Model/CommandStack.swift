//
//  CommandStack.swift
//  socketTEST
//
//  Created by Myroniv on 24.04.18.
//  Copyright © 2018 sasha. All rights reserved.
//

import Foundation
import SwiftSocket

class CommandStack{
    
    var stack: [Int] = Array<Int>()
    var next: Bool = false
    var tmpdata = [UInt8]()
    var listenerIsWorking: Bool = false
    
    let client = TCPClient(address: "31.47.84.131", port: 8899)
    init(){
        switch client.connect(timeout: 5) {
        case .success:
            print("connection success")
        case .failure(let error):
            print(error)
            
        }
    }
    
    func addCommand(id: Int){
        stack.append(id)
    }
    
    func sendCommand(){
        if(stack.count > 0){
            let frame = TableID.sharedInstanse.getFrameById(id: stack[0])
            var request: [UInt8] = []
            let comand = Formatter(deviceID: frame.deviceId)
            request = comand.generateReadComand()
            print(request)
            switch client.send(data: request) {
                  case .success:
                        tmpdata = [UInt8]()
                        while(true){
                            let data = client.read(1024*8 , timeout: 2)
                            if(data != nil){
                                tmpdata += data!
                                print(tmpdata)
                                if(dataParser(data: &tmpdata)){
                                    if stack.count > 0 {
                                        stack.remove(at: 0)
                                    }
                                    sendCommand()
                                    break;
                                }
                            }
                        }
                        case .failure(let error):
                            print(error)
                            sendCommand()
                        }
                }
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateInternal"), object: nil)
        }
    
    func dataParser(data: inout [UInt8]) -> Bool{
        //Check stream bytes for -- or -+
        var len: Int = 0
        var ddata = data
        var endcount = ddata.count - 1
        var index = 0
        while(index < endcount){
            if(ddata[index] == 0x2D){
                switch ddata[index+1]{
                case 0x2D:
                    ddata.remove(at: index)
                    endcount -= 1
                case 0x2B:
                    ddata.remove(at: index)
                    endcount -= 1
                default:
                    break
                }
            }
            index += 1
        }
        
        while(ddata.count > 0){
            if(ddata[0] == 0x2B){ // Start byte?
                if(ddata.count > 1){ //We have a type of responce
                    let responcetype = responceType(type: ddata[1])
                    if(responcetype){
                        if(ddata.count > 2){
                            switch ddata[1]{
                            case 0x05:
                                len = Int(ddata[2]) + 5
                            case 0x06:
                                //Need at least 2 bytes for calculation responce lenght
                                if(ddata.count > 3){
                                    len = Converter.parseInt(mas: ddata, offset: 2) + 6
                                } else{
                                    len = 0
                                }
                            default:
                                len = 0
                            }
                           
                            if(len <= ddata.count){
                                let responce: [UInt8] = Array(ddata[0...(len-1)])
                                print("Parser work with frame = \(responce)")
                                Parser.sharedInstance.setData(data: responce)
                                if(Parser.sharedInstance.checkCrc()){
                                    Parser.sharedInstance.saveData()
                                } else{
                                    print("Bad CRC for this frame ")
                                    print("Input data = \(ddata)")
                                    print("Cleaning bad frame")
                                    for _ in 0...(len-1){
                                        ddata.remove(at: 0)
                                    }
                                    print("Result data without frame(bad CRC) = \(ddata)")
                                    continue
                                }
                            } else{
                                print("Part of frame1")
                                return false
                            }
                        } else{
                            print("Havent lenght")
                            return false
                        }
                    } else{
                        print("Bad responce type in frame")
                        ddata.remove(at: 0)
                        ddata.remove(at: 0)
                        print("Data after clening to first start = \(ddata)")
                        continue
                    }
                } else{
                    print("Part of frame2")
                    ddata.remove(at: 0)
                    return false
                }

            } else{
                print("Packet havnt start byte")
                for _ in 0..<ddata.count{
                    if(ddata[0] != 0x2B){
                        ddata.remove(at: 0)
                    } else{
                        print("Data after clening to first start = \(ddata)")
                        break
                    }
                }
                continue
            }
            for _ in 0...(len-1){
                ddata.remove(at: 0)
            }
        }
        
        return true
    }
    
    
    func listener(){
        if(!listenerIsWorking){
            listenerIsWorking = true
            tmpdata = [UInt8]()
            var bool = true
            while(bool){
                let data = self.client.read(1024 , timeout: 2)
                print("Simple read= \(data)")
                if(data != nil){
                    self.tmpdata += data!
                    let res = self.dataParser(data: &self.tmpdata)
                    if(!res){
                        continue
                    }
                    print("Send Notification Update External")
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateExternal"), object: nil)
                }
                if((stack.count > 0)){
                    sendCommand()
                    print("Send Notification Update Internal")
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateInternal"), object: nil)
                }
                bool = false
            }
            
            listenerIsWorking = false
        }
    }
    
    func responceType(type: UInt8) -> Bool{
        switch type{
        case 0x05:
            return true
        case 0x06:
            return true
        default:
            return false
        }
    
    }
    
}

