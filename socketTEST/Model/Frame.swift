//
//  Frame.swift
//  socketTEST
//
//  Created by Myroniv on 23.04.18.
//  Copyright © 2018 sasha. All rights reserved.
//

import Foundation

class Frame{
    var id: Int
    var description: String = ""
    var deviceId: [UInt8]
    var type: Int
    // 1 - Float
    // 2 - String
    // 3 - Unsigned char
    // 4 - UnsignedInt32
    var data:[UInt8]?
    var refreshed: Bool = false
    var updateTime:Int = 60
    
    
    init(id: Int,description: String,deviceId: [UInt8],type: Int){
        self.id = id
        self.description = description
        self.deviceId = deviceId
        self.type = type
    }
    
    func getResult() -> String{
        var s = ""
        switch type {
        case 1:
            s =  String(format: "%.2f", parseFloat())
        case 2:
            s =  "\(parseString())"
        case 3:
            s =  "\(parseUnsignedChar())"
        case 4:
            s =  "\(parseUnsignedInt32())"
        default:
            s = ""
        }
        return s
    }
    
    func parseFloat()->Float{
        if self.data == nil {
            return 0.0
        }
        
        if (data![3] == 219 && data![4] == 45 && data![5] == 45 && data![6] == 105 && data![7] == 174) {
            return Converter.parseFloat32(mas: self.data!, offset: 8)
        }
        
        return Converter.parseFloat32(mas: self.data!, offset: 7)
    }
    
    func parseString()->String{
        if self.data == nil {
            return ""
        }
        var newdata = data![7..<((data?.count)!)]
        for i in 7..<(data?.count)!{
            if(data?[i] == 0){
                newdata = data![7..<i]
                break
            }
        }
        return String(bytes: newdata, encoding: .ascii)!
    }
    
    func parseInt()->Int{
        if self.data == nil {
            return 0
        }
        return Converter.parseInt(mas: self.data!, offset: 7)
    }
    
    func parseUnsignedInt32()->UInt{
        if self.data == nil {
            return 0
        }
        return Converter.parseUnsignedInt32(mas: self.data!, offset: 7)
    }
    
    func parseUnsignedChar()->String{
        if self.data == nil {
            return ""
        }
        return Converter.parseUnsignedChar(mas: self.data!, offset: 7)
    }
    
    
}
