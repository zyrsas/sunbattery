//
//  Parser.swift
//  socketTEST
//
//  Created by Myroniv on 23.04.18.
//  Copyright © 2018 sasha. All rights reserved.
//

import Foundation

class Parser{
    var data: [UInt8]
    
    static let sharedInstance = Parser()
    
     private init(){
        self.data = []
    }
    
    func setData(data: [UInt8]){
        self.data = data
    }
    
    func checkCrc() -> Bool{
        
        var CRC: [UInt8]  = []
        var len: Int = 0
        switch data[1]{
            case 0x05:
                len = Int(data[2]) + 2
            case 0x06:
                len = Converter.parseInt(mas: data, offset: 2) + 3
            default:
                len = 0
        }
        
        CRC = Array(data[1...len])
        if(CRC.count&1 == 1){
            CRC.append(0)
        }
        let serverCRC:[UInt8] = Array(data[(len+1)...(len+2)])
        let CRC_calc: [UInt8] = CRC16.instance.getCRCResult(by: CRC)
        if(CRC_calc == serverCRC){
            return true;
        } else{
            return false;
        }
        
    }
    
    func saveData(){
       
        
        let deviceId:Array<UInt8> = [data[3], data[4], data[5], data[6]]

        for item in TableID.sharedInstanse.responceTable{
            if (item.deviceId == deviceId){
                item.data = data
                item.refreshed = true
                return
                
            }
        }

       
    }
}
