//
//  TableID.swift
//  socketTEST
//
//  Created by sasha on 4/21/18.
//  Copyright © 2018 sasha. All rights reserved.
//

import Foundation

class TableID {
    
    static let sharedInstanse = TableID()
    
    var responceTable:[Frame] = Array<Frame>()
    
    init() {
        //type of parameter can be 
        // 1 - Float
        // 2 - String
        // 3 - Unsigned char
        // 4 - UnsignedInt32
        responceTable.append(Frame(id: 0,description: "DC input A voltage [V]_0xB298395D",deviceId:[0xB2,0x98,0x39,0x5D],type: 1))
        responceTable.append(Frame(id: 1,description: "DC input A power [W]_0xDB11855B",deviceId:[0xDB, 0x11, 0x85, 0x5B],type: 1))
        responceTable.append(Frame(id: 2,description: "DC input B voltage [V]_0x5BB8075A",deviceId:[0x5B,0xB8,0x07,0x5A],type: 1))
        responceTable.append(Frame(id: 3,description: "DC input B power[W]_0x0CB5D21B",deviceId:[0x0C, 0xB5, 0xD2, 0x1B],type: 1))
        responceTable.append(Frame(id: 4,description: "Battery voltage [V]_0xA7FA5C5D",deviceId:[0xA7, 0xFA, 0x5C, 0x5D],type: 1))
        responceTable.append(Frame(id: 5,description: "Battery State of Charge (SoC)_0x959930BF",deviceId:[0x95, 0x99, 0x30, 0xBF],type: 1))
        responceTable.append(Frame(id: 6,description: "Target SOC_0x8B9FF008",deviceId:[0x8B, 0x9F, 0xF0, 0x08],type: 1))
        responceTable.append(Frame(id: 7,description: "Battery power(positive if discharge) [W]_0x400F015B",deviceId:[0x40, 0x0F, 0x01, 0x5B],type: 1))
        responceTable.append(Frame(id: 8,description: "Battery temperature [°C]_0x902AFAFB",deviceId:[0x90,0x2A,0xFA,0xFB],type: 1))
        responceTable.append(Frame(id: 9,description: "External power(G) [W]_0xE96F1844",deviceId:[0xE9, 0x6F, 0x18, 0x44],type: 1))
        responceTable.append(Frame(id: 10,description: "Public grid power [W]_0x91617C58",deviceId:[0x91, 0x61, 0x7C, 0x58],type: 1))
        responceTable.append(Frame(id: 11,description: "Load in household_0x1AC87AA0",deviceId:[0x1A, 0xC8, 0x7A, 0xA0],type: 1))
        responceTable.append(Frame(id: 12,description: "Inverter actual state_0x5F33284E",deviceId:[0x5F, 0x33, 0x28, 0x4E],type: 3))
        responceTable.append(Frame(id: 13,description: "Grid power enable _0x36A9E9A6",deviceId:[0x36, 0xA9, 0xE9, 0xA6],type: 3))
        responceTable.append(Frame(id: 14,description: "Inverter serial number_0x7924ABD9",deviceId:[0x79, 0x24, 0xAB, 0xD9],type: 2))
        responceTable.append(Frame(id: 15,description: "Name of appliance_0xEBC62737",deviceId:[0xEB, 0xC6, 0x27, 0x37],type: 2))
        responceTable.append(Frame(id: 16, description: "Actual inverters AC-power [W]_0xDB2D69AE", deviceId: [0xDB, 0x2D, 0x69, 0xAE], type: 1))
        
    
    }

    func checkNeedUpdate() -> Bool{
        for item in responceTable{
            if(!item.refreshed){
                return true
            }
        }
        return false
    }
    
    func getFrameById(id: Int)-> Frame{
        return responceTable[id]
    }
    
    func cleaned() {
        for item in responceTable {
            item.refreshed = false
            
        }
        if(responceTable[14].getResult() == ""){
            responceTable[14].refreshed = false
        } else{
            responceTable[14].refreshed = true
        }
        if(responceTable[15].getResult() == ""){
            responceTable[15].refreshed = false
        } else {
            responceTable[15].refreshed = true
        }
    }
}
